@extends('User.layout.master')
@section('Title')
    الاختبار
    @stop

@section('content')
    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>{{$course->course_title}} Exam</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">
                    <div id="tabsleft" class="tabbable tabs-left">
                        <ul>
                            @foreach($exams as $exam)
                            <li><a href="#tabsleft-tab{{$exam->id}}" data-toggle="tab">{{$exam->id}}</a></li>
                                @endforeach
                        </ul>
                        <div id="bar" class="progress progress-info progress-striped">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                        </div>
                        <form action="{{route('POST_FINISH_EXAM',$course->id)}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                        <div class="tab-content">
                            @foreach($exams as $exam)
                            <div class="tab-pane" id="tabsleft-tab{{$exam->id}}">
                                <div class="quest text-right">
                                    <div class="quest-head">
                                        <h1>{{$exam->exam_title}}</h1>
                                    </div>


                                    <!-- end quest-head -->
                                    <div class="quest-answers">
                                        @foreach($exam->Answers as $answer)
                                        <div class="answer">
                                            <label>
                                                <input type="radio" name="answers[{{$answer}}]" id="make-answer" value="{{$answer->id}}">
                                                <span>{{$answer->answer}}</span>
                                            </label>
                                        </div>
                                    @endforeach

                                        <!-- end answer -->
                                    </div>
                                    <!-- end quest-answers -->
                                </div>
                                <!-- end quest -->
                            </div>
                            @endforeach

                            <ul class="pager wizard">
                                <!--                        <li class="previous first"><a href="javascript:;">First</a></li>-->
                                <li class="previous"><a href="javascript:;">الخطوة السابقة</a></li>
                                <!--                        <li class="next last"><a href="javascript:;">Last</a></li>-->
                                <li class="next"><a href="javascript:;">الخطوة التالية</a></li>
                                <li class="next finish" style="display:none;"><button type="submit" class="btn btn-success" style="float: right;width: 120px ; border: none">انهاء</button></li>
                            </ul>
                        </div>
                        </form>
                    </div>
                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>
    @stop



   @section('UserScripts')

     <script src="{{asset('User/js/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $('#tabsleft').bootstrapWizard({
            'tabClass': 'nav nav-tabs',
            'debug': false,
            onTabShow: function (tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;
                var $percent = ($current / $total) * 100;
                $('#tabsleft .progress-bar').css({
                    width: $percent + '%'
                });

                // If it's the last tab then hide the last button and show the finish instead
                if ($current >= $total) {
                    $('#tabsleft').find('.pager .next').hide();
                    $('#tabsleft').find('.pager .finish').show();
                    $('#tabsleft').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#tabsleft').find('.pager .next').show();
                    $('#tabsleft').find('.pager .finish').hide();
                }

            }
        });

        $('#tabsleft .finish').click(function () {
            alert('Finished!, Starting over!');
            $('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
        });
    </script>

    @stop