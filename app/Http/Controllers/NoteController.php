<?php

namespace App\Http\Controllers;

use App\Course;
use App\Note;
use Illuminate\Http\Request;
use App\Setting;


class NoteController extends Controller
{
    public function postAddNote(Request $request)
    {
        $this->validate($request, ['note_title' => 'required', 'note_description' => 'required']);
        $note = new Note();
        $note->user_id = $request->user_id;
        $note->course_id = $request->course_id;
        $note->note_title = $request->note_title;
        $note->note_description = $request->note_description;
        $note->save();
        return back()->with('success', 'تم اضافه التنبيه');
    }

    public function getCourseNotes($id)
    {
        $settings = Setting::all();

        $course = Course::findOrFail($id);
        $notes = Note::where('course_id', $course->id)->get();
        return view('User.Courses.Notes', compact('course', 'notes', 'settings'));
    }
}
