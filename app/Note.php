<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = ['note_title', 'note_description', 'user_id', 'course_id'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
