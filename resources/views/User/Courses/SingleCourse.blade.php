@extends('User.layout.master')
@section('Title')
    {{$course->course_title}}
@stop
@section('content')

    <div class="intro-container col-xs-12">
        <div class="intro-head text-center">
            <div class="container">
                <h1>{{$course->course_title}}</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.intro-head -->

        <div class="corse-box col-xs-12">
            <div class="corse-nav text-center">
                <div class="container">
                    <ul>
                        @if((\Auth::id())==$course->teacher_id)
                            <li>
                                <a href="#" class="add-alert-form">
                                    <i class="fa fa-bullhorn"></i> إضافة تنويه
                                </a>
                            </li>
                            <li>
                                <a href="#" class="sent-all">
                                    <i class="fa fa-envelope"></i> إرسال للجميع
                                </a>
                            </li>
                        @endif
                        <li>
                            <a href="#" class="active">
                                <i class="fa fa-tasks"></i> الدروس
                            </a>
                        </li>

                        <li>
                        <li>
                            <a href="{{route('GET_COURSE_DISCUSS',$course->id)}}">
                                <span class="padge">{{\App\Discuss::count()}}</span>
                                <i class="fa fa-commenting-o"></i> النقاشات
                            </a>
                        </li>

                        <li>
                            <a href="{{route('GET_COURSE_NOTES',$course->id)}}">
                                <span class="padge">{{\App\Note::count()}}</span>

                                <i class="fa fa-bell"></i> التنويهات
                            </a>
                        </li>
                        {{--<li>--}}
                            {{--<a href="{{route('ADD_TO_FAV',$course->id)}}" class="add-fav" data-toggle="tooltip"--}}
                               {{--data-placement="top" title="إضافة الي المفضلة">--}}
                                {{--<i class="fa fa-heart"></i>--}}
                            {{--</a>--}}
                            {{--<a href="{{route('REMOVE_FROM_FAV',$course->id)}}" class="add-fav-dis" data-toggle="tooltip" data-placement="top"--}}
                               {{--title="عدم التفضيل">--}}
                                {{--<i class="fa fa-heart"></i>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        <li class="rating" data-toggle="tooltip" data-placment="top" title="إضافة تقييم للدورة">
                            <ul>
                                <li>
                                    <a href="{{route('FIRST_ADD_RATE',$course->id)}}">
                                        <i class="fa fa-star active"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('SECOND_ADD_RATE',$course->id)}}">
                                        <i class="fa fa-star active"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('THIRD_ADD_RATE',$course->id)}}">
                                        <i class="fa fa-star-half-o active"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('FORTH_ADD_RATE',$course->id)}}">
                                        <i class="fa fa-star"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('FIFTH_ADD_RATE',$course->id)}}">
                                        <i class="fa fa-star"></i>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- =========================================================================================================================================== -->

                    <div class="panel-pop modal" id="msg-all">
                        <form method="post" action="{{route('POST_SEND_MESSAGE',$course->id)}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="lost-inner">
                                <h1>
                                    <i class="fa fa-envelope"></i>
                                    إرسال لجميع الطلاب المشتركين في الدورة
                                </h1>

                                <div class="lost-item" id="messageTo">
                                    <textarea name="message" placeholder="اكتب الرسالة هنا"></textarea>
                                </div>
                                <!-- /.lost-item -->
                                <div class="text-center">
                                    <input type="submit" value="إرسال">
                                </div>

                                <!-- /.lost-item -->
                            </div>
                        </form>
                        <!-- /.lost-inner -->
                    </div>
                    <!-- /.modal -->

                    <!-- =========================================================================================================================================== -->
                    <form method="post" action="{{route('POST_SEND_NOTE')}}">
                        {{csrf_field()}}
                        <div class="panel-pop modal" id="alert-all">
                            <div class="lost-inner">
                                <h1>
                                    <i class="fa fa-envelope"></i>
                                    اضافة تنويه للطلاب المشتركين في الدورة
                                </h1>
                                <input type="hidden" value="{{\Auth::user()->id}}" name="user_id">
                                <input type="hidden" value="{{$course->id}}" name="course_id">
                                <div class="lost-item" id="alert-item">
                                    <input type="text" name="note_title" placeholder="عنوان التنويه">
                                </div>
                                <!-- /.lost-item -->
                                <div class="lost-item" id="alert-item">
                                    <textarea name="note_description" placeholder="مضمون التنويه"></textarea>
                                </div>
                                <!-- /.lost-item -->
                                <div class="text-center">
                                    <input type="submit" value="نشر التنويه">
                                </div>
                                <!-- /.lost-item -->
                            </div>
                            <!-- /.lost-inner -->
                        </div>
                    </form>
                    <!-- /.modal -->

                    <!--
                </form>
                    =========================================================================================================================================== -->
                </div>
                <!-- end container -->
            </div>
            <!-- end corse-nav -->
            <div class="lesson-box text-right">
                <div class="container">
                {{--<div class="certf text-center animated bounceIn">--}}
                {{--<h1>تهانينا لقد  انتهيت من هذه الدورة بنجاح </h1>--}}
                {{--<a href="#">--}}
                {{--<i class="fa fa-print"></i> تستطيع طباعة الشهادة--}}
                {{--</a>--}}
                {{--</div>--}}
                <!-- end certf -->
                    @if(count($lectures)==null)
                        <div class="empty-msg text-center animated shake">
                            <h1>
                                <i class="fa fa-frown-o"></i>
                                لا يوجد دروس الان ولكن يمكنك الاشتراك في الدورة لحين بدأها
                            </h1>
                        </div>
                    @else
                    <!-- end empty-msg -->
                        <div class="week-module text-right">
                            <h1>
                                <i class="fa fa-tasks"></i>
                                الاسبوع الاول
                            </h1>
                        </div>
                        <!-- end week-moduke -->
                        <ul>
                            @foreach($lectures as $lecture)
                                <li>
                                    <a href="{{route('GET_SINGLE_LECTURE',$lecture->id)}}" class="lesson-det">
                                        <i class="fa fa-play-circle"></i>
                                        <span class="lesson-name">{{$lecture->lecture_title}}</span>
                                    </a>
                                    <h3>{{$lecture->lecture_time}} </h3>
                                    @if(Auth::id()==$lecture->teacher_id)
                                        <a href="{{route('GET_DELETE_LECTURE',$lecture->id)}}" class="del-lesson"
                                           data-toggle="tooltip" data-placement="top" title="حذف الدرس">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    @endif
                    <div class="take-exam col-xs-12 text-center">
                        {{--<a href="test-show.html">--}}
                        {{--<i class="fa fa-file-text-o"></i> ابدا الاختبار الان--}}
                        {{--</a>--}}
                    </div>
                    <!-- end take-exam -->
                </div>
                <!-- end container -->
            </div>
            <!-- end lesson-box -->
        </div>
        <!-- end corse-box -->

    </div>

@stop
