@extends('User.layout.master')
@section('Title')
    أضافه كورس
@stop

@section('content')
    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>إضافة دورة جديدة</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">

                    <form action="{{route('POST_ADD_COURSE')}}" method="post" class="add-form"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="Auth" value="{{Auth::user()->id}}">
                        <div class="up_form-item">
                            <h1>عنوان الدورة</h1>
                            <input type="text" name="course_title" placeholder="اضف عنوان الدورة">
                        </div>
                        <div class="up_form-item">
                            <h1>تاريخ بدايه الدوره</h1>
                            <input type="date" name="course_date" class="form-control">
                        </div>
                        <div class="up_form-item">
                            <h1>تاريخ انتهاء الدوره</h1>
                            <input type="date" name="course_end" class="form-control">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>متطلب سابق</h1>
                            <input type="text" name="pre_exp" class="form-control">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>المجال</h1>
                            <select name="cat_id">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->title_ar}}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1> رابط الفيديو والصوره </h1>
                            <div class="add_cont text-right">
                                <div class="lecture-item">
                                    <div class="add_cont text-right">
                                        <label class="text-right">
                                            <input type="checkbox" id="up-video">
                                            <span>اذا أردت رفع فيديو وصوره من جهازك الشخصي</span>
                                        </label>

                                        <div class="videoUploaded col-xs-12 text-right">
                                            <span><i class="fa fa-video-camera"></i> ارفع فيديو من جهازك</span>
                                            <input type="file" name="course_video" class="uploaded">
                                        </div>
                                        <div class="videoUploaded col-xs-12 text-right">
                                            <span><i class="fa fa-camera"></i> ارفع صوره من جهازك</span>
                                            <input type="file" name="course_image" class="uploaded">
                                        </div>

                                    </div>

                                </div>
                                <!-- /.lecture-item -->
                            </div>
                            <input type="text" name="course_video_url" placeholder="ادخل رابط فيديو" class="linked">

                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>وصف الدورة</h1>
                            <textarea name="course_description" placeholder="اضف وصف الدورة"></textarea>
                        </div>


                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>الجنس المتوقع</h1>
                            <div class="add_cont text-right">
                                <label class="text-right">
                                    <input type="checkbox" value="1" name="gender">
                                    <span>ذكور</span>
                                </label>
                                <label class="text-right">
                                    <input type="checkbox" value="2" name="gender">
                                    <span>إناث</span>
                                </label>
                            </div>
                        </div>

                        <div class="up_form-item">
                            <h1>نوع الدورة</h1>
                            <div class="add_cont text-right">
                                <label class="text-right">
                                    <input type="radio" value="1" name="course_payment_method" id="r_1"
                                           onclick="javascript:check();">
                                    <span>مجاني</span>
                                </label>
                                <label class="text-right">
                                    <input type="radio" value="2" name="course_payment_method" id="r_2"
                                           onclick="javascript:check()">
                                    <span>مدفوع</span>
                                </label>
                                <input type="number" name="course_salary" id="course_salary" data-toggle="tooltip"
                                       data-placement="top" title="اضف سعر الدورة">
                            </div>
                        </div>
                        <!-- /.up_form-item -->

                        <div class="up_form-item">
                            <a class="add-cert">اضافة شهادة للدورة</a>
                            <div class="course-cert">
                                <div class="up_form-item">
                                    <h1>إسم الشهادة</h1>
                                    <input type="text" name="certificate_name">
                                </div>
                                <!-- /.up_form-item -->
                                <div class="up_form-item">
                                    <h1>الجهة المانحة</h1>
                                    <input type="text" name="certificate_branch">
                                </div>
                                <!-- /.up_form-item -->
                                <div class="up_form-item">
                                    <h1>تكلفة الشهادة</h1>
                                    <div class="add_cont text-right">
                                        <label class="text-right">
                                            <input type="radio" value="1" name="certificate_payment" id="c_1"
                                                   onclick="javascript:cer_check()">
                                            <span>مجاني</span>
                                        </label>
                                        <label class="text-right">
                                            <input type="radio" value="2" name="certificate_payment"
                                            id="c_2" onclick="javascript:cer_check()">
                                            <span>مدفوع</span>
                                        </label>
                                        <input type="number" id="certificate_salary" name="certificate_salary" data-toggle="tooltip"
                                               data-placement="top"
                                               title="اضف سعر الشهادة"></div>
                                </div>
                                <!-- /.up_form-item -->
                            </div>
                            <!-- /.course-cert -->
                        </div>
                        <!-- /.up_form-item -->

                        <div class="up_form-item up-confirm">
                            <input type="submit" value="اضافة الدورة">
                        </div>
                        <!-- /.up_form-item -->
                    </form>

                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>


@stop
<script type="text/javascript">

    function check() {
        if (document.getElementById('r_2').checked) {
            document.getElementById('course_salary').style.visibility = 'visible';
        }
        else {
            document.getElementById('course_salary').style.visibility = 'hidden';
        }
    }

    function cer_check() {
        if (document.getElementById('c_2').checked) {
            document.getElementById('certificate_salary').style.visibility = 'visible';
        }
        else {
            document.getElementById('certificate_salary').style.visibility = 'hidden';
        }
    }

</script>