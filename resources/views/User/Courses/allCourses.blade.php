@extends('User.layout.master')
@section('Title')
    جميع الدورات
@stop
@section('content')

    <div class="allcourses-box">
        <div class="allcourses-head text-center">
            <div class="container">
                <h1>جميع الكورسات</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.allcourses-head -->
        <div class="search-categories text-center">
            <div class="container">
                <div class="cat-item">
                    <div class="cat-inner col-md-6 col-sm-6 col-xs-6 pull-right">
                        <a href="#" class="show-cat">
                            <?php
                            $course = DB::table('courses')->first();
                            echo $course->course_title;
                            ?>
                            <i class="fa fa-caret-down"></i></a>
                        <div class="hidden-cat">
                            <ul>
                                @foreach($courses as $course)
                                    <li>
                                        <a href="{{route('GET_THIS_COURSE',$course->id)}}">{{$course->course_title}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /. cat-inner -->
                    <div class="cat-inner col-md-6 col-sm-6 col-xs-6 pull-left">
                        <form method="post" action="{{route('SEARCH_FOR_COURSES')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="search" name="course_search" placeholder="ابحث عن كورسات أخري">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                    <!-- /. cat-inner -->
                </div>
                <!-- /.cat-item -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.search-categories -->
        <div class="allcourses-body">
            <div class="container">
                <div class="row">
                    <div class="row block-container">
                        @foreach($courses as $course)
                            <div class="block col-md-4">
                                <figure>

                                    <div><img src="{{asset('User/images/'.$course->course_image)}}" alt="image"
                                              class="img-responsive"></div>
                                    <figcaption class="text-right">
                                        <h1>اسم الكورس : {{$course->course_title}}</h1>
                                        <h1>اسم المدرس
                                            : {{\App\User::where('id',$course->teacher_id)->first()->fullName}}  </h1>
                                        <h1>عدد الطلبة المشتركة : <?php
                                            $students = \Illuminate\Support\Facades\DB::table('courses_users')->select('user_id')->where('course_id', $course->id)->count();
                                            echo $students;
                                            ?>
                                        </h1>
                                        <h1>تاريخ بدايه المحاضره : {{$course->course_date}}</h1>
                                        <h1>
                                            :تقييم الكورس
                                            <?php
                                            $items = \App\Rates::where('course_id', $course->id)->count('rate');
                                            $all = \App\Rates::where('course_id', $course->id)->sum('rate');
                                            if (!$items == 0) {
                                                $sum = $all / $items;
                                            } else {
                                                $sum = 0;
                                            }
                                            for ($i = 0; $i < $sum; $i++) {
                                                echo ' <i class="fa fa-star active" style="color: yellow"></i>';

                                            }
                                                echo '<i class="fa fa-star-half-full" style="color: yellow"></i>'
                                            ?></h1>
                                        @if((\Auth::id())==$course->teacher_id)
                                            <a href="{{route('GET_ADD_LECTURES',$course->id)}}">
                                                <i class="fa fa-eye"></i> أضافه محاضرات
                                            </a>
                                        @endif
                                        <a href="{{route('GET_THIS_COURSE',$course->id)}}">
                                            <i class="fa fa-eye"></i>مشاهده الكورس
                                        </a>
                                    </figcaption>
                                </figure>
                            </div>
                            <!-- /.block -->
                        @endforeach


                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.row -->

                <div class="inner col-xs-12 text-center">
                    <ul class="pagination">
                        <li>
                            {{--{{ $courses->links() }}--}}
                        </li>
                    </ul>
                </div>
                <!-- end inner -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /.allcourses-body -->
    </div>

@stop

@section('UserScripts')

@stop