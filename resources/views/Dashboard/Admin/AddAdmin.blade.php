@extends('Dashboard.layout.master')
@section('content')
    <h3>اضافه أدمن جديد</h3>
    <hr>

    <form method="post" action="{{route('POST_ADD_ADMIN')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="admin_name">اسم الادمن</label>
            <br>
            <input type="text" name="name" class="form-control" id="admin_name" placeholder="اسم الادمن ...">
        </div>
        <div class="form-group">
            <label for="email">الايميل </label>
            <br>

            <input type="email" name="email" class="form-control" id="email" placeholder="الايميل">
        </div>
        <div class="form-group">
            <label for="password">الباسورد </label>
            <br>
            <input type="password" name="password" class="form-control" id="password">
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
    </form>
@stop