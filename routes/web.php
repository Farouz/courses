<?php
Route::get('/', 'UsersController@index')->name('home');
//login and sign up and sign out
Route::get('/sign-up', 'UsersController@getSignUp')->name('GET_SIGNUP');
Route::post('/sign-up', 'UsersController@postSignUp')->name('POST_SIGNUP');
Route::post('/sign-in', 'UsersController@postSigIn')->name('POST_LOGIN');
Route::get('/logout', 'UsersController@logout')->name('LOG_OUT');
//verify
Route::get('/verify/{token}', 'VerifyController@verify')->name('verify');
Route::get('/about', 'UsersController@getAbout')->name('GET_ABOUT');
Route::get('/contact', 'ContactsController@getcontact')->name('GET_CONTACT');
Route::post('/contact', 'ContactsController@postcontact')->name('POST_ADD_MESSAGE');
Route::get('/policity', 'UsersController@getPolicity')->name('GET_POLICITY');
Route::group(['middleware' => 'guest'], function () {
    //profile
    Route::get('/user-profile/{id}', 'UsersController@getUserProfile')->name('GET_USER_PROFILE');
    Route::post('/editProfile/{id}', 'UsersController@postEditProfile')->name('POST_EDIT_PROFILE');
    Route::post('/reset-password/{id}', 'UsersController@postRestPassword')->name('POST_CHANGE_PASSWORD');
    Route::post('/addCV', 'UsersController@postAddCv')->name('POST_ADD_CV');
    Route::post('/addInterest', 'UsersController@postAddInterest')->name('POST_ADD_INTEREST');
    Route::get('/deleteInterest/{id}', 'UsersController@DeleteInterest')->name('DELETE_INTEREST');

    //Courses Routes

    Route::get('/course/{id}', 'CourseController@getSingleCourse')->name('GET_THIS_COURSE');
    Route::get('/beginCourse/{id}', 'CourseController@getStartCourse')->name('GET_START_COURSE');
    Route::get('/edit-course/{id}', 'CourseController@getEditCourse')->name('GET_EDIT_COURSE');
    Route::post('/edit-course/{id}', 'CourseController@postEditCourse')->name('POST_EDIT_COURSE');
    Route::get('/addCourse', 'CourseController@getAddCourse')->name('GET_ADD_COURSE');
    Route::post('/addCourse', 'CourseController@postAddCourse')->name('POST_ADD_COURSE');
    Route::get('/allCourses', 'CourseController@getAllCourses')->name('GET_ALL_COURSES');
    Route::get('/deleteCourse/{id}', 'CourseController@deleteCourse')->name('DELETE_THIS_COURSE');
    Route::get('/deleteCoursePivote/{id}', 'CourseController@removeCoursePivote')->name('POST_REMOVE_COURSE_FROM_PIVOTE');
    Route::post('/Courses-search', 'SearchController@postSearchCourse')->name('SEARCH_FOR_COURSES');


//Lectures Routes
    Route::get('/addLecture', 'LecturesController@getAddLecture')->name('GET_ADD_LECTURE');
    Route::get('/addLectures/{id}', 'LecturesController@getAddLectures')->name('GET_ADD_LECTURES');
    Route::post('/addLecture', 'LecturesController@postAddLecture')->name('POST_ADD_LECTURE');
    Route::get('/delete-lecture/{id}', 'LecturesController@getDeleteLecture')->name('GET_DELETE_LECTURE');
    Route::get('/lecture/{id}', 'LecturesController@getSingleLecture')->name('GET_SINGLE_LECTURE');

    //exams
    Route::get('/addExam/{id}', 'ExamsController@getAddExam')->name('GET_ADD_EXAM');
    Route::post('/addExam', 'ExamsController@postAddExam')->name('POST_ADD_EXAM');
    Route::get('/theExam/{id}', 'ExamsController@getTheExam')->name('GET_THE_EXAM');
    Route::post('/finish-exam/{id}', 'ExamsController@postFinishExam')->name('POST_FINISH_EXAM');


    //categories
    Route::get('/allCategories', 'CourseController@getAllCategories')->name('GET_ALL_COURSES_CATEGORIES');
    Route::get('/category/{id}', 'CourseController@getCategory')->name('GET_CATEGORY');

    //notes and discuss
    Route::post('/add-note', 'NoteController@postAddNote')->name('POST_SEND_NOTE');
    Route::post('/add-discuss', 'CourseController@postAddDiscuss')->name('POST_ADD_DISCUSS');
    Route::get('/course-notes/{id}', 'NoteController@getCourseNotes')->name('GET_COURSE_NOTES');
    Route::get('/course-discuss/{id}', 'CourseController@getCoursediscuss')->name('GET_COURSE_DISCUSS');
    Route::get('/finish/{id}', 'LecturesController@getFinishCourse')->name('GET_FINISH_COURSE');

    //messages from contact
    Route::get('/send-messages/{id}', 'MailsController@postSendMessages')->name('POST_SEND_MESSAGE');
    //Rates
    Route::get('/add-rate-one/{id}', 'RatesController@addRateOne')->name('FIRST_ADD_RATE');
    Route::get('/add-rate-two/{id}', 'RatesController@addRateTwo')->name('SECOND_ADD_RATE');
    Route::get('/add-rate-three/{id}', 'RatesController@addRateThree')->name('THIRD_ADD_RATE');
    Route::get('/add-rate-four/{id}', 'RatesController@addRateFour')->name('FORTH_ADD_RATE');
    Route::get('/add-rate-five/{id}', 'RatesController@addRateFive')->name('FIFTH_ADD_RATE');
    Route::get('/dislike-rate/{id}', 'RatesController@dislikeRate')->name('DISLIKE_RATE');
    //payment routes
    Route::get('/buy-course/{id}', 'ShopController@buyCourse')->name('GET_BUY_COURSE');
    Route::get('/get-done', 'ShopController@getDone')->name('GET_DONE');
    Route::get('/get-cancel', 'ShopController@getCancel')->name('GET_CANCEL');

   //favs
    Route::get('add-fav/{id}', 'RatesController@addToFav')->name('ADD_TO_FAV');
    Route::get('remove-fav/{id}', 'RatesController@RemoveFav')->name('REMOVE_FROM_FAV');

});
//Admin Routes


Route::get('/get-dashboard-login', 'AdminController@getDashboardLogin')->name('DASHBOARD_LOGIN');
Route::get('/changeLanguage/{lang}', 'LanguageController@getChangeLanguage')->name('CHANGE_LANGUAGE')->middleware('lang');

Route::post('/dashb-login', 'AdminController@postDashboardLogin')->name('POST_DASHBOARD_LOGIN');
Route::group(['prefix' => 'dashboard', 'middleware' => 'AuthAdmin:webadmin'], function () {
    Route::get('/', 'AdminController@getDashboard')->name('Dashboard');
    Route::get('/logout', 'AdminController@getlogout')->name('GET_ADMIN_LOGOUT');
    Route::get('/addCategory', 'CourseCategoryController@getAddCategory')->name('GET_ADD_CATEGORY');
    Route::post('/addCategory', 'CourseCategoryController@postAddCategory')->name('POST_ADD_CATEGORY');
    Route::get('/allCategory', 'CourseCategoryController@getAllCategory')->name('GET_ALL_CATEGORIES');
    Route::get('/deleteCategory/{id}', 'CourseCategoryController@getDeleteCategory')->name('GET_DELETE_CATEGORY');
    Route::get('/allCourses', 'DashCoursesController@getAllCourses')->name('GET_ALL_COURSES_DASH');
    Route::get('/deleteCourse/{id}', 'DashCoursesController@DeleteCourse')->name('GET_DELETE_COURSE_DASH');
    Route::get('/approveCourse/{id}', 'DashCoursesController@getApproveCourse')->name('APPROVE_COURSE');
    Route::get('/addCourse', 'DashCoursesController@getAddCourse')->name('GET_ADD_COURSE_DASH');
    Route::post('/addCourse', 'DashCoursesController@postAddCourse')->name('POST_ADD_COURSE_DASH');
    Route::get('/editCourse/{id}', 'DashCoursesController@getEditCourse')->name('GET_EDIT_COURSE_DASH');
    Route::post('/editCourse/{id}', 'DashCoursesController@postEditCourse')->name('POST_EDIT_COURSE_DASH');
    Route::get('/showCourse/{id}', 'DashCoursesController@getShowCourse')->name('GET_SHOW_COURSE_DASH');


    Route::get('/addLectures/{id}', 'DashLecturesController@getAddLectures')->name('ADD_LECTURES_DASH');
    Route::get('/show-details/{id}', 'DashLecturesController@getThisLectureDetails')->name('GET_LECTURE_DETAILS_DASH');
    Route::post('/addLectures', 'DashLecturesController@postAddLectures')->name('POST_ADD_LECTURE_DASH');
    Route::get('/allLectures', 'DashLecturesController@getAllLectures')->name('GET_ALL_LECTURES_DASH');
    Route::get('/editLecture/{id}', 'DashLecturesController@getEditLecture')->name('GET_EDIT_LECTURE_DASH');
    Route::post('/editLecture/{id}', 'DashLecturesController@postEditLecture')->name('POST_EDIT_LECTURE_DASH');
    Route::get('/deleteLecture', 'DashLecturesController@DeleteLeture')->name('GET_DELETE_LECTURE_DASH');
    Route::get('/approveLecture', 'DashLecturesController@approveLeture')->name('APPROVE_LECTURE');
    Route::get('/relatedLectures/{id}', 'DashLecturesController@getrelatedLeture')->name('RELATED_LECTURES');
    Route::get('/setup-settings', 'SettingsController@getAddSettings')->name('GET_SETUP_SETTINGS');
    Route::post('/setup-settings', 'SettingsController@postAddSettings')->name('POST_ADD_SETTINGS');
    Route::get('/show-settings', 'SettingsController@getShowSettings')->name('GET_SHOW_SETTINGS');
    Route::get('/update-settings/{id}', 'SettingsController@getEditSettings')->name('EDIT_SETTING');
    Route::post('/update-settings/{id}', 'SettingsController@postEditSettings')->name('POST_EDIT_SETTINGS');
    Route::get('/add-customer', 'CustomersController@getAddCustomers')->name('GET_ADD_USERS');
    Route::post('/add-customer', 'CustomersController@postAddCustomers')->name('POST_ADD_CUSTOMER');
    Route::get('/all-customer', 'CustomersController@getAllCustomers')->name('GET_ALL_CUSTOMERS');
    Route::get('/delete-customer/{id}', 'CustomersController@deleteCustomer')->name('GET_DELETE_CUSTOMER');
    Route::get('/allAdmins', 'AdminController@getAllAdmins')->name('GET_ALL_ADMINS');
    Route::get('/AddAdmins', 'AdminController@getAddAdmins')->name('GET_ADD_ADMIN');
    Route::post('/AddAdmins', 'AdminController@postAddAdmins')->name('POST_ADD_ADMIN');
    Route::get('/deleteAdmin/{id}', 'AdminController@deleteAdmin')->name('GET_DELETE_ADMIN');
    Route::get('/editAdmin/{id}', 'AdminController@getEditAdmin')->name('GET_EDIT_ADMIN');
    Route::post('/editAdmin/{id}', 'AdminController@postEditAdmin')->name('POST_EDIT_ADMIN');
    Route::get('editCategory/{id}', 'CourseCategoryController@getEditCategory')->name('GET_EDIT_CATEGORY');
    Route::post('editCategory/{id}', 'CourseCategoryController@postEditCategory')->name('POST_EDIT_CATEGORY');
    Route::get('editUser/{id}', 'DashUsersController@getEditUser')->name('GET_EDIT_USER');
    Route::post('editUser/{id}', 'DashUsersController@postEditUser')->name('POST_EDIT_USER');
    Route::get('addUser', 'DashUsersController@getAddUser')->name('GET_ADD_USER');
    Route::post('addUser', 'DashUsersController@postAddUser')->name('POST_ADD_USER');
    Route::get('/getProfile/{id}', 'DashUsersController@getUserProfile')->name('GET_THIS_PROFILE');
    Route::get('/allUsers', 'DashUsersController@getAllUsers')->name('GET_ALL_USERS');
    Route::get('/deleteUser/{id}', 'DashUsersController@getDeleteUser')->name('DELETE_USER');
    Route::get('/ApproveUser/{id}', 'DashUsersController@getApproveUser')->name('APPROVE_USER');

});