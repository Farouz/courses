@extends('Dashboard.layout.master')
@section('content')
    <h3>Edit Category</h3>
    <hr>
    <form method="post" action="{{route('POST_EDIT_CATEGORY',$category->id)}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="catTitle">Category Title</label>
            <input type="text" name="title_en" class="form-control" id="catTitle" value="{{$category->title_en}}">
        </div>
        <div class="form-group">
            <label for="CatTitleAr">اسم المجال</label>
            <input type="text" name="title_ar" class="form-control" id="CatTitleAr" value="{{$category->title_ar}}">
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
    </form>
@stop