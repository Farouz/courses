<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function getAddSettings()
    {
        return view('Dashboard.Setting.setup');
    }

    public function postAddSettings(Request $request)
    {
        $this->validate($request, ['site_name' => 'required', 'site_image' => 'required|mimes:jpeg,jpg,png', 'about_image' => 'required|mimes:jpeg,jpg,png', 'facebook' => 'required|url', 'twitter' => 'required|url', 'linkden' => 'required|url', 'description' => 'required', 'policity' => 'required',]);
        $setting = new Setting();
        $setting->site_name = $request->site_name;
        $setting->facebook = $request->facebook;
        $setting->twitter = $request->twitter;
        $setting->linkden = $request->linkden;
        $setting->description = $request->description;
        $setting->policity = $request->policity;
        if ($request->hasFile('site_image')) {
            $image_name = md5($request->site_image->getClientOriginalName()) . '.' . $request->site_image->getClientOriginalExtension();
            $request->site_image->move(public_path('User/images/'), $image_name);
            $setting->site_image = $image_name;
        } else {
            $setting->site_image = 'logo.png';
        }
        if ($request->hasFile('about_image')) {
            $image_about = md5($request->about_image->getClientOriginalName()) . '.' . $request->about_image->getClientOriginalExtension();
            $request->about_image->move(public_path('User/images/'), $image_about);
            $setting->about_image = $image_about;
        } else {
            $setting->about_image = 'logo.png';
        }
        $setting->save();
        return redirect()->route('GET_SHOW_SETTINGS')->with('success', 'Setting Added');

    }

    public function getShowSettings()
    {
        $settings = Setting::all();
        return view('Dashboard.Setting.Settings', compact('settings'));
    }

    public function getEditSettings($id)
    {
        $setting = Setting::findOrFail($id);
        return view('Dashboard.Setting.EditSettings', compact('setting'));
    }

    public function postEditSettings($id, Request $request)
    {
        $this->validate($request, ['site_image' => 'mimes:jpeg,jpg,png', 'about_image' => 'mimes:jpeg,jpg,png', 'facebook' => 'url', 'twitter' => 'url', 'linkden' => 'url']);
        $setting = Setting::find($id);
        $setting->site_name = $request->site_name;
        $setting->facebook = $request->facebook;
        $setting->twitter = $request->twitter;
        $setting->linkden = $request->linkden;
        $setting->description = $request->description;
        $setting->policity = $request->policity;
        if ($request->hasFile('site_image')) {
            $image_name = md5($request->site_image->getClientOriginalName()) . '.' . $request->site_image->getClientOriginalExtension();
            $request->site_image->move(public_path('User/images/'), $image_name);
            $setting->site_image = $image_name;
        }
        if ($request->hasFile('about_image')) {
            $about_image = md5($request->about_image->getClientOriginalName()) . '.' . $request->about_image->getClientOriginalExtension();
            $request->about_image->move(public_path('User/images/', $about_image));
            $setting->about_image = $about_image;
        }
        $setting->save();
        return redirect()->route('GET_SHOW_SETTINGS')->with('success', 'Setting Updated ');
    }
}
