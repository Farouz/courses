@extends('Dashboard.layout.master')
@section('content')
    <strong>
        <center>الملف الشخصي الخاص ب {{$user->userName}}</center>
    </strong>
    <br>
    <br>
    <br>
<center><p>الصوره الشخصيه</p></center>
    <div class="home_img  text-center">
        <div class="home_img-inner">
            <div class="left-caption col-xs-12">
                <div class="imgcontent col-xs-12">
                    <div class="bstext">
                    </div>
                </div>
                <!-- /.imgcontent -->
            </div>
            <!-- /.left-caption -->
            <div>
                <img src="{{asset('public/User/images/'.$user->Image)}}" alt="" width="150"
                     height="150">
            </div>

        </div>
    </div>
    <!-- /.home_img -->
    <div class="home-content">

        <div class="home_data col-md-10 col-sm-10 col-xs-12 text-right">
            <form action="#" method="get">
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-user-secret"></i>
                        <h1>الإسم بالكامل</h1>

                        <span>{{$user->fullName}}</span>
                    </div>
                </div>
                <!-- /.home_data-item -->

                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-user"></i>
                        <h1>إسم المستخدم</h1>
                        <span>{{$user->userName}}</span>
                    </div>
                </div>
                <!-- /.home_data-item -->
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-phone"></i>
                        <h1>رقم الهاتف</h1>

                        <span>{{$user->phone}}</span>
                    </div>
                </div>
                <!-- /.home_data-item -->

                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-envelope"></i>
                        <h1>البريد الإلكتروني</h1>
                        <span>{{$user->email}}</span>
                    </div>
                </div>
                <!-- /.home_data-item -->
                <div class="home_data-item col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-globe"></i>
                        <h1>الدولة</h1>

                        <span>{{$user->country}}</span>
                    </div>
                </div>
                <!-- /.home_data-item -->
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-male"></i>
                        <h1>الجنس</h1>

                        <?php
                        if ($user->gender == 1) {
                            echo '<span> مذكر </span>';
                        } else {
                            echo '<span>مؤنث </span>';
                        }
                        ?>
                    </div>
                </div>
                <!-- /.home_data-item -->
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-globe"></i>
                        <h1>مدرب / متدرب</h1>
                        <?php
                        if ($user->is_teacher == 1) {
                            echo ' <span>مدرب</span>';
                        } else {
                            echo ' <span>متدرب</span>';

                        }
                        ?>
                    </div>
                </div>
                <!-- /.home_data-item -->

                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-graduation-cap"></i>
                        <h1> المؤهل</h1>
                        <span>{{$user->qualification}}</span>
                    </div>
                </div>
                <!-- /.home_data-item -->

                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-briefcase"></i>
                        <h1>التخصص</h1>

                        <span>{{$user->spcialization}}</span>
                    </div>
                </div>
                <!-- /.home_data-item -->

                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-cogs"></i>
                        <h1>مجال العمل</h1>
                        <span>{{$user->job_title}} </span>
                    </div>
                </div>
                <!-- /.home_data-item -->
            </form>

                <!-- /.home_data-item -->
        </div>
        <!-- ./home_data -->
    </div>
    <!-- /.home-content -->
   <a href="{{route('GET_ALL_USERS')}}"> <button class="btn btn-success" style="float: left;width: 180px;height: 40px">Back To All Users</button></a>
   <a href="{{route('GET_EDIT_USER',$user->id)}}"> <button class="btn btn-success" style="float: right;width: 180px;height: 40px">Edit This Profile </button></a>

@stop