<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseCategory extends Model
{
    protected $fillable = ['title_en', 'title_ar'];

    public function Courses()
    {
        return $this->hasMany(Course::class, 'cat_id');
    }
}
