<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fav extends Model
{
    protected $fillable=[
      'user_id','course_id'
    ];
    protected $table='users_favs';
}
