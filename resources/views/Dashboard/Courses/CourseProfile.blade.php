@extends('Dashboard.layout.master')
@section('content')
    <strong>
        <center>الملف الشخصي الخاص ب {{$course->courseName}}</center>
    </strong>
    <br>
    <br>
    <br>
    <strong>
        <center><p style="font-size: large">الفيديو الخاص بالدوره </p></center>
    </strong>
    <div class="home_img  text-center">
        <div class="home_img-inner">
            <div class="left-caption col-xs-12">
                <div class="imgcontent col-xs-12">
                    <div class="bstext">
                    </div>
                </div>
                <!-- /.imgcontent -->
            </div>
            <!-- /.left-caption -->
            <div>
                <div class="media">
                    <div class="media-body">
                        <?php
                        if (!$course->course_video_url == null) {
                            $video = Embed::make($course->course_video_url)->parseUrl();
                            if ($video) {
                                $video->setAttribute(['width' => 450]);
                                echo $video->getHtml();
                            }
                        } else {
                            $video = Embed::make($course->course_video)->parseUrl();
                            if ($video) {
                                $video->setAttribute(['width' => 450]);
                                echo $video->getHtml();
                            }
                        }

                        ?>
                    </div>
                </div>


            </div>

        </div>
    </div>
    <strong>
        <center><p style="font-size: large">الصوره الخاص بالدوره </p></center>
    </strong>

    <div class="home_img  text-center">
        <div class="home_img-inner">
            <div class="left-caption col-xs-12">
                <div class="imgcontent col-xs-12">
                    <div class="bstext">
                    </div>
                </div>
                <!-- /.imgcontent -->
            </div>
            <!-- /.left-caption -->

            <div>
                <img src="{{asset('public/User/images/'.$course->course_image)}}" alt="" width="350"
                     height="200">
            </div>

        </div>
    </div>
    <!-- /.home_img -->
    <div class="home-content">

        <div class="home_data col-md-10 col-sm-10 col-xs-12 text-right">
            <form action="#" method="get">
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-comment"></i>
                        <h1>اسم الدوره</h1>

                        <span>{{$course->course_title}}</span>
                    </div>
                </div>
                <!-- /.home_data-item -->

                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-youtube-play"></i>
                        <h1>اسم القسم</h1>
                        <span>{{App\CourseCategory::where('id',$course->cat_id)->first()->title_en}}</span>
                    </div>
                </div>
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-user"></i>
                        <h1>اسم المدرس</h1>
                        <span>{{App\User::where('id',$course->teacher_id)->first()->fullName}}
                            &nbsp as &nbsp &nbsp {{App\User::where('id',$course->teacher_id)->first()->userName}} </span>
                    </div>
                </div>
                <!-- /.home_data-item -->
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-paste"></i>
                        <h1>متطلب سابق</h1>

                        <span>{{$course->pre_exp}}</span>
                    </div>
                </div>
                <!-- /.home_data-item -->

                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-user"></i>
                        <h1>الجنس </h1>
                        <span>@if($course->gender == 1)
                                مذكر
                            @elseif($course->gender==2 )
                                مؤنث
                            @else
                                مذكر ومؤنث
                            @endif
                        </span>
                    </div>
                </div>
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-money"></i>
                        <h1> نوعيه الدفع</h1>
                        <span>@if($course->course_payment_method == 1)
                                مجاني
                            @else
                                مدفوع
                            @endif
                        </span>
                    </div>
                </div>
                <!-- /.home_data-item -->
                @if($course->course_payment_method==2)
                    <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                        <div>
                            <i class="fa fa-dollar"></i>
                            <h1>سعر الدوره</h1>

                            <span>{{$course->course_salary}} </span> <i class="fa fa-dollar"></i>
                        </div>
                    </div>
                @endif
            <!-- /.home_data-item -->

                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-calendar-check-o"></i>
                        <h1>وقت انشاء الدوره</h1>
                        <span>{{$course->created_at->toDateString()}}</span>
                    </div>
                </div>
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-calendar"></i>
                        <h1>وقت بدايه الدوره</h1>
                        <span>{{$course->course_date}}</span>
                    </div>
                </div>
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-calendar-o"></i>
                        <h1>وقت نهايه الدوره</h1>
                        <span>{{$course->course_end}}</span>
                    </div>
                </div>
                @if(isset($course->certificate_name))
                    <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                        <div>
                            <i class="fa fa-cogs"></i>
                            <h1>وقت نهايه الدوره</h1>
                            <span>{{$course->certificate_name}}</span>
                        </div>
                    </div>
                    <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                        <div>
                            <i class="fa fa-cogs"></i>
                            <h1>وقت نهايه الدوره</h1>
                            <span>{{$course->certificate_branch}}</span>
                        </div>
                    </div>
                    <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                        <div>
                            <i class="fa fa-cogs"></i>
                            <h1>وقت نهايه الدوره</h1>
                            <span>{{$course->certificate_salary}}</span>
                        </div>
                    </div>
                @endif
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-commenting-o"></i>
                        <h1>عن الدوره</h1>
                        <div style="font-size: large;">{{$course->course_description}}</div>
                    </div>
                </div>

                <!-- /.home_data-item -->
            </form>
            <!-- /.home_data-item -->
        </div>
        <!-- ./home_data -->
        <!-- /.home-content -->
        <a href="{{route('GET_ALL_COURSES_DASH')}}" style="float: left;"><button class="btn btn-success">Back To ALL Courses</button></a>
        <a href="{{route('GET_EDIT_COURSE_DASH',$course->id)}}" style="float: right;"><button class="btn btn-success">Edit This Course</button></a>
    </div>

@stop
