<?php

namespace App\Http\Controllers;

use App\Course;
use foo\bar;
use Netshell\Paypal\Facades\Paypal;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\PaypalControlle;

class ShopController extends Controller
{
    public function buyCourse($id)
    {
        $course = Course::findOrFail($id);
        $item=\Session::put('variable',$course);
        if (!empty($course)) {
            $paypal_class = new PaypalControlle();
            return $paypal_class->getCheckout('USD', $course->course_title, $course->course_description, $course->course_salary);

        } else {
            return back();
        }
    }

    public function getDone(Request $request)
    {
        $item=\Session::get('variable');
        $id = $request->get('paymentId');
        $token = $request->get('token');
        $payer_id = $request->get('PayerID');
        $paypal_class = new PaypalControlle();
        $user=\Auth::user();
        $user->Courses->attach($item->id);
        $method = $paypal_class->check_payment($id, $token, $payer_id);
        if (!empty($method->state) or isset($method->state)) {
            return redirect()->route('GET_THIS_COURSE',$item->id)->with('method', $method);
        }
        else{
            return redirect()->route('GET_THIS_COURSE',$item->id);
        }
    }

    public function getCancel()
    {

    }
}
