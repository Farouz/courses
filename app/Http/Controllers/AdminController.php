<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function getDashboard()
    {

        return view('Dashboard.Home.index');
    }

    public function getDashboardLogin()
    {
        return view('Dashboard.Auth.loginForm');
    }

    public function postDashboardLogin()
    {
        if (!auth()->guard('webadmin')->attempt(\request(['email', 'password']))) {
            return back()->withErrors(['message' => 'Email Or Password Not Correct']);
        };
        return redirect()->route('Dashboard')->with('success', 'Welcome ');
    }

    public function getlogout()
    {
        \auth()->guard('webadmin')->logout();
        return redirect()->route('DASHBOARD_LOGIN');
    }

    public function getAllAdmins()
    {
        $admins = Admin::all();
        return view('Dashboard.Admin.AllAdmin', compact('admins'));
    }

    public function getAddAdmins()
    {
        return view('Dashboard.Admin.AddAdmin');
    }

    public function postAddAdmins(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required|email', 'password' => 'required']);
        $admin = new Admin();
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->save();
        return back()->with('success', 'Admin Added');

    }

    public function getEditAdmin($id)
    {
        $admin = Admin::findOrFail($id);
        return view('Dashboard.Admin.EditAdmin', compact('admin'));
    }

    public function postEditAdmin($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required|email', 'password' => 'required']);
        $admin = Admin::findOrFail($id);
        if ($request->password == $admin->password) {
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->password = bcrypt($request->password);
            $admin->save();
            return redirect()->route('GET_ALL_ADMINS')->with('success', 'profile updated');
        } else {
            return redirect()->route('GET_ALL_ADMINS')->with('error', 'Password incorrect');
        }
    }

    public function deleteAdmin($id)
    {
        Admin::destroy($id);
        return back()->with('success', 'Admin Removed');
    }
}
