<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $table = 'certificates';
    protected $fillable = ['user_id', 'course_id', 'certificate_name', 'certificate_branch', 'certificate_salary', 'certificate_payment'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
