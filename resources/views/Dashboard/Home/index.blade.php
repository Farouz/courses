@extends('Dashboard.layout.master')
@section('content')
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-files-o" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All Course Categories</span>
                <span class="info-box-number">{{App\CourseCategory::count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-book" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All Course</span>
                <span class="info-box-number">{{App\Course::count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-blue-gradient"><i class="fa fa-video-camera" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All lectures</span>
                <span class="info-box-number">{{App\Lecture::count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green-active"><i class="fa fa-users" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All Users</span>
                <span class="info-box-number">{{App\User::count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-user" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All Teachers</span>
                <span class="info-box-number">{{App\User::where('is_teacher',1)->count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-maroon-active"><i class="fa fa-user" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All Students</span>
                <span class="info-box-number">{{App\User::where('is_teacher',0)->count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-blue-active"><i class="fa fa-certificate" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All Certificates</span>
                <span class="info-box-number">{{App\Certificate::count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red-active"><i class="fa fa-inbox" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All Messages</span>
                <span class="info-box-number">{{App\Contact::count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua-gradient"><i class="fa fa-paperclip" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All CVs</span>
                <span class="info-box-number">{{App\CV::count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-light-blue-gradient"><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All Courses Discusses</span>
                <span class="info-box-number">{{App\Discuss::count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-sticky-note" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All Courses Notes</span>
                <span class="info-box-number">{{App\Note::count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange-active"><i class="fa fa-users" aria-hidden="true"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">All People Who talks about us </span>
                <span class="info-box-number">{{App\Talk::count()}}</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
@stop