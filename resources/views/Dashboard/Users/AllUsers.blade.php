@extends('Dashboard.layout.master')
@section('content')
    <h4>All Users</h4>
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">All Users</h3>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>الاسم بالكامل</th>
                        <th>الايميل</th>
                        <th>رقم التليفون</th>
                        <th>مدرس - طالب</th>
                        <th>حاله التسحيل</th>
                        <th> مصرح له</th>
                        <th>التحكم</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->fullName}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->phone}}</td>
                            <td>
                                <?php
                                if ($user->is_teacher == 1) {
                                    echo 'مدرس';
                                } elseif ($user->is_teacher == 0) {
                                    echo 'طالب';
                                }
                                ?>
                            </td>
                            <td><?php
                                if ($user->active == 1) {
                                    echo 'مفعل';
                                } else {
                                    echo 'غير مفعل';
                                }
                                ?></td>
                            <th>
                                <?php
                                if ($user->approved == '1') {
                                    echo 'تم التصريح';
                                } else {
                                    echo 'لم يتم التصريح';
                                }

                                ?>
                            </th>

                            <td>
                                <a href="{{route('DELETE_USER',$user->id)}}"><i class="fa fa-trash fa-lg"
                                                                                style="color: red;"
                                                                                data-toggle="tooltip"
                                                                                title="Delete User"></i></a>
                                &#8209;
                                @if(!$user->approved == 1)
                                <a href="{{route('APPROVE_USER',$user->id)}}"><i class="fa fa-check-square-o"
                                                                                 data-toggle="tooltip"
                                                                                 style="color: green"
                                                                                 title="Approve User"></i></a>
                                &#8209;
                                @endif
                                <a
                                        href="{{route('GET_EDIT_USER',$user->id)}}"><i class="fa fa-edit"
                                                                                       data-toggle="tooltip"
                                                                                       style="color: green"
                                                                                       title="Edit User"></i></a>
                                &#8209;
                                <a
                                        href="{{route('GET_THIS_PROFILE',$user->id)}}"><i class="fa fa-user"
                                                                                       data-toggle="tooltip"
                                                                                       style="color: green"
                                                                                       title="Profile User"></i></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>
            </div><!-- /.box-body -->
        </div>
    </section>
@stop

@section('scripts')
    <script src="{{asset('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/app.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop