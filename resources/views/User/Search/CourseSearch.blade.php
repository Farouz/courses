@extends('User.layout.master')
@section('Title')
    نتائج البحث
    @stop
@section('content')
    <div class="courses">
        <div class="container">
            <div class="courses-head text-center">
                <h1>نتائج البحث</h1>
            </div>
            <!-- /.testominal-head -->
            <div class="row block-container">
                @foreach($courses as $course)
                    <div class="block col-md-4 col-sm-6">
                        <figure>
                            <div><img src="{{asset('User/images/'.$course->course_image)}}" alt="img05" class="img-responsive"></div>
                            <figcaption class="text-right">
                                <h1>
                                    <label>اسم الكورس</label>
                                    <span>{{$course->course_title}}</span>
                                </h1>
                                <h1>
                                    <label>اسم المدرس</label>
                                    <span>{{App\User::where('id',$course->teacher_id)->first()->fullName}}</span>

                                </h1>
                                <h1>
                                    <label>عدد الطلبة المشتركة</label>
                                    <span><?php
                                        $students = \Illuminate\Support\Facades\DB::table('courses_users')->select('user_id')->where('course_id',$course->id)->count();
                                        echo $students;
                                        ?></span>

                                </h1>
                                <h1>
                                    <label>تاريخ بداية الكورس</label>
                                    <span> {{$course->course_date}}</span>

                                </h1>
                                <h1>
                                    <label>تقييم الكورس</label>
                                    <span>جيد</span>

                                </h1>
                                <a href="{{route('GET_THIS_COURSE',$course->id)}}">
                                    <i class="fa fa-eye"></i> مشاهدة الكورس
                                </a>
                            </figcaption>
                        </figure>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@stop