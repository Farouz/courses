@extends('Dashboard.layout.master')
@section('CssFiles')

    <link href="{{asset('User/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/selectric.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/reset.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/images/favicon.png')}}" rel="icon" type="text/css">

@stop
@section('content')
    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>تعديل دوره </h1>
                <p style="color: white;font-size: large">{{$course->course_title}}</p>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">

                    <form action="{{route('POST_EDIT_COURSE_DASH',$course->id)}}" method="post" class="add-form"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="up_form-item">
                            <h1>عنوان الدورة</h1>
                            <input type="text" name="course_title" value="{{$course->course_title}}">
                        </div>
                        <div class="up_form-item">
                            <h1>المدرب</h1>
                            <select name="teacher_id">
                                <option value="{{$course->teacher_id}}">{{App\User::where('id',$course->teacher_id)->first()->fullName}}
                                    &nbsp; as &nbsp;
                                    &nbsp; {{App\User::where('id',$course->teacher_id)->first()->userName}}</option>
                                @foreach($teachers as $teacher)
                                    <option value="{{$teacher->id}}">{{$teacher->fullName}} &nbsp; as &nbsp;
                                        &nbsp;{{$teacher->userName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="up_form-item">
                            <h1>تاريخ بدايه الدوره</h1>
                            <input type="date" name="course_date" class="form-control" value="{{$course->course_date}}">
                        </div>
                        <div class="up_form-item">
                            <h1>تاريخ انتهاء الدوره</h1>
                            <input type="date" name="course_end" class="form-control" value="{{$course->course_end}}">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>متطلب سابق</h1>
                            <input type="text" name="pre_exp" class="form-control" value="{{$course->pre_exp}}">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>المجال</h1>
                            <select name="cat_id">
                                <option value="{{$course->cat_id}}">{{App\CourseCategory::where('id',$course->cat_id)->first()->title_ar}}</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->title_ar}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="up_form-item">
                            <h1> رابط الفيديو والصوره </h1>
                            <div class="add_cont text-right">
                                <div class="lecture-item">
                                    <div class="add_cont text-right">
                                        <label class="text-right">
                                            <input type="checkbox" id="up-video">
                                            <span>اذا أردت رفع فيديو وصوره من جهازك الشخصي</span>
                                        </label>

                                        <div class="videoUploaded col-xs-12 text-right">
                                            <span><i class="fa fa-video-camera"></i> ارفع فيديو من جهازك</span>
                                            <input type="file" name="course_video" class="uploaded"
                                                   value="{{$course->course_video}}">
                                        </div>
                                        <div class="videoUploaded col-xs-12 text-right">
                                            <span><i class="fa fa-camera"></i> ارفع صوره من جهازك</span>
                                            <input type="file" name="course_image" class="uploaded"
                                                   value="{{$course->course_image}}">
                                        </div>

                                    </div>

                                </div>
                                <!-- /.lecture-item -->
                            </div>
                            <input type="text" name="course_video_url" placeholder="ادخل رابط فيديو" class="linked"
                                   value="{{$course->course_video_url}}">
                            <small>Just Copy YouTube Url  </small>

                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>وصف الدورة</h1>
                            <textarea name="course_description">{{$course->course_description}}</textarea>
                        </div>


                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>الجنس المتوقع</h1>
                            <div class="add_cont text-right">
                                <label class="text-right">
                                    <input type="checkbox" value="1" name="gender"
                                           @if($course->gender==1) checked @endif>
                                    <span>ذكور</span>
                                </label>
                                <label class="text-right">
                                    <input type="checkbox" value="2" name="gender"
                                           @if($course->gender==2) checked @endif>
                                    <span>إناث</span>
                                </label>
                            </div>
                        </div>
                        <div class="up_form-item">
                            <h1>نوع الدورة</h1>
                            <div class="add_cont text-right">
                                <label class="text-right">
                                    <input type="radio" value="1" name="course_payment_method" id="radio_1"
                                           onclick="javascript:yesnoCheck();"
                                           @if($course->course_payment_method == 1) checked @endif>
                                    <span>مجاني</span>
                                </label>
                                <label class="text-right">
                                    <input type="radio" value="2" name="course_payment_method" id="radio_2"
                                           onclick="javascript:yesnoCheck();"
                                           @if($course->course_payment_method) checked @endif>
                                    <span>مدفوع</span>
                                </label>
                                <input type="number" id="salary" name="course_salary" data-toggle="tooltip"
                                       data-placement="top"
                                       title="اضف سعر الدورة">
                            </div>
                        </div>
                        <!-- /.up_form-item -->

                        <div class="up_form-item">
                            <a class="add-cert">اضافة شهادة للدورة</a>
                            <div class="course-cert">
                                <div class="up_form-item">
                                    <h1>إسم الشهادة</h1>
                                    <input type="text" name="certificate_name" value="{{$course->certificate_name}}">
                                </div>
                                <!-- /.up_form-item -->
                                <div class="up_form-item">
                                    <h1>الجهة المانحة</h1>
                                    <input type="text" name="certificate_branch"
                                           value="{{$course->certificate_branch}}">
                                </div>
                                <!-- /.up_form-item -->
                                <div class="up_form-item">
                                    <h1>تكلفة الشهادة</h1>
                                    <div class="add_cont text-right">
                                        <label class="text-right">
                                            <input type="radio" value="1" name="certificate_payment" id="cer_1"
                                                   onclick="javascript:certificateCheck();"
                                                   @if($course->certificate_payment == 1) checked @endif>
                                            <span>مجاني</span>
                                        </label>
                                        <label class="text-right">
                                            <input type="radio" value="2" id="cer_2"
                                                   onclick="javascript:certificateCheck();"
                                                   name="certificate_payment"
                                                   @if($course->certificate_payment==2) checked @endif>
                                            <span>مدفوع</span>
                                        </label>
                                        <input type="number" id="cer_salary" name="certificate_salary"  data-toggle="tooltip"
                                               data-placement="top"
                                               title="اضف سعر الشهادة"></div>
                                </div>
                                <!-- /.up_form-item -->
                            </div>
                            <!-- /.course-cert -->
                        </div>
                        <!-- /.up_form-item -->

                        <div class="up_form-item up-confirm">
                            <input type="submit" value="اضافة الدورة">
                        </div>
                        <!-- /.up_form-item -->
                    </form>

                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>




@stop
<script src="{{asset('User/js/jquery-2.2.2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/html5shiv.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery-smoothscroll.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/modernizr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/placeholdem.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/toucheffects.js')}}"></script>
<script src="{{asset('User/js/jquery.selectric.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/classie.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery.nicescroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/script.js')}}" type="text/javascript"></script>

<script type="text/javascript">

    function yesnoCheck() {
        if (document.getElementById('radio_2').checked) {
            document.getElementById('salary').style.visibility = 'visible';
        }
        else {
            document.getElementById('salary').style.visibility = 'hidden';
        }
    }

    function certificateCheck() {
        if (document.getElementById('cer_2').checked) {
            document.getElementById('cer_salary').style.visibility = 'visible';
        }
        else {
            document.getElementById('cer_salary').style.visibility = 'hidden';
        }
    }

</script>