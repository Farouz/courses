@extends('Dashboard.layout.master')
@section('content')
    <h4>All Courses</h4>
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">All Courses</h3>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>أسم الدوره</th>
                        <th>أسم المدرس</th>
                        <th>القسم</th>
                        <th>حاله السماح</th>
                        <th>التحكم</th>
                    </tr>
                    </thead>
                    @foreach($courses as $course)
                        <tbody>

                        <td>{{$course->course_title}}</td>
                        <td>{{App\User::where('id',$course->teacher_id)->first()->fullName}} &#8209;({{App\User::where('id',$course->teacher_id)->first()->userName}})</td>
                        <td>{{\App\CourseCategory::where('id',$course->cat_id)->first()->title_ar}}</td>
                        <td>@if($course->approved == 1)
                        تم السماح
                        @else
                            لم يتم السماح بعد
                                @endif
                        </td>
                        <td>
                            <a href="{{route('GET_DELETE_COURSE_DASH',$course->id)}}">
                                <i class="fa fa-trash fa-lg" data-toggle="tooltip" style="color: red"
                                   title="Delete Course "></i>
                            </a>
                            @if($course->approved == 0)
                                <a href="{{route('APPROVE_COURSE',$course->id)}}">
                                    <i class="fa fa fa-check-square-o" data-toggle="tooltip" style="color: green"
                                       title="Approve The Course"></i>
                                </a>
                            @endif
                            <a href="{{route('GET_SHOW_COURSE_DASH',$course->id)}}"><i class="fa fa-desktop"
                                                                                    data-toggle="tooltip"
                                                                                    style="color: darkblue"
                                                                                    title="Show Course"></i>
                            </a><a href="{{route('ADD_LECTURES_DASH',$course->id)}}"><i class="fa  fa-plus-circle fa-lg"
                                                                                        data-toggle="tooltip"
                                                                                        style="color: darkblue"
                                                                                        title="Add Lectures"></i>
                            </a>
                            <a href="{{route('RELATED_LECTURES',$course->id)}}"><i class="fa  fa-film"
                                                                                   data-toggle="tooltip"
                                                                                   style="color: darkblue"
                                                                                   title="Show related lectures"></i>
                            </a>

                        </tbody>
                    @endforeach
                </table>
            </div><!-- /.box-body -->
        </div>
    </section>
@stop

@section('scripts')
    <script src="{{asset('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/app.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop