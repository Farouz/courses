@extends('User.layout.master')
@section('Title')
    {{$category->title_ar}}
@stop
@section('content')

    <div class="allcourses-box">
        <div class="allcourses-head text-center">
            <div class="container">
                <h1>{{ $category->title_ar}}</h1>

            </div>
            <!-- /.container -->
        </div>
        <!-- /.allcourses-head -->
        <div class="search-categories text-center">
            <div class="container">
                <div class="cat-item">
                    <div class="cat-inner col-md-6 col-sm-6 col-xs-6 pull-right">
                        <a href="#" class="show-cat">{{ $category->title_ar}} <i class="fa fa-caret-down"></i></a>
                        <div class="hidden-cat">
                            <ul>
                                @foreach($categories as $item)
                                    <li>
                                        <a href="{{route('GET_CATEGORY',$item->id)}}">{{$item->title_ar}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /. cat-inner -->
                    <div class="cat-inner col-md-6 col-sm-6 col-xs-6 pull-left">
                        <form>
                            <input type="search" placeholder="ابحث عن كورسات أخري">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                    <!-- /. cat-inner -->
                </div>
                <!-- /.cat-item -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.search-categories -->
        <div class="allcourses-body">
            <div class="container">
                <div class="row">
                    <div class="row block-container">
                        @if(count($courses) == null)
                            <div class="empty-msg text-center animated shake">
                                <h1>
                                    <i class="fa fa-frown-o"></i>
                                    لا محاضرات لهذا القسم
                                </h1>
                            </div>
                        @else()
                        @foreach($courses as $cours)
                            <div class="block col-md-4">
                                <figure>
                                    <div><img src="{{asset('User/images/'.$cours->course_image)}}" alt="img05"
                                              class="img-responsive"></div>
                                    <figcaption class="text-right">
                                        <h1>اسم الكورس : {{$cours->title_ar}}</h1>
                                        <h1>اسم المدرس
                                            : {{\App\User::where('id',$cours->teacher_id)->first()->fullName}}</h1>
                                        <h1>عدد الطلبة المشتركة</h1>
                                        <h1>تاريخ بدايه الكورس : {{$cours->course_date}}</h1>
                                        <h1>تقييم الكورس</h1>

                                        @if((\Auth::id())==$cours->teacher_id)
                                            <a href="{{route('GET_ADD_LECTURES',$cours->id)}}">
                                                <i class="fa fa-eye"></i> أضافه محاضرات
                                            </a>
                                            <a href="{{route('GET_THIS_COURSE',$cours->id)}}">
                                                <i class="fa fa-eye"></i> مشاهده
                                            </a>
                                            @else
                                            <a href="{{route('GET_THIS_COURSE',$cours->id)}}">
                                                <i class="fa fa-eye"></i> مشاهده الكورس
                                            </a>
                                        @endif
                                    </figcaption>
                                </figure>
                            </div>
                            <!-- /.block -->
                        @endforeach
                        @endif
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.row -->

                <div class="inner col-xs-12 text-center">
                    <ul class="pagination">
                        <ul class="pagination">
                            <li>
                                {{ $courses->links() }}
                            </li>
                        </ul>
                    </ul>
                </div>
                <!-- end inner -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /.allcourses-body -->
    </div>

@stop