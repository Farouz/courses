@extends('User.layout.master')
@section('Title')
    تعديل المحاضره
@stop
@section('AboveScripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("strong").hide();
        });
    </script>
@stop
@section('content')


    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>{{$course->course_title}}</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">

                    <form action="{{route('POST_EDIT_COURSE',$course->id)}}" method="post" class="add-form"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="Auth" value="{{Auth::user()->id}}">
                        <div class="up_form-item">
                            <h1>عنوان الدورة</h1>
                            <input type="text" name="course_title" value="{{$course->course_title}}">
                        </div>
                        <div class="up_form-item">
                            <h1>تاريخ بدايه الدوره</h1>
                            <input type="date" name="course_date" value="{{$course->course_date}}" class="form-control">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>متطلب سابق</h1>
                            <input type="text" name="pre_exp" value="{{$course->pre_exp}}" placeholder="إسم المتطلب">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>المجال</h1>
                            <select name="cat_id">
                                @foreach($categories as $category)
                                    <option value="{{$course->cat_id}}">{{$cc=\App\CourseCategory::where('id',$course->cat_id)->first()->title_ar}}</option>
                                    <option value="{{$category->id}}">{{$category->title_ar}}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1> رابط الفيديو والصوره </h1>
                            <div class="add_cont text-right">
                                <div class="lecture-item">
                                    <div class="add_cont text-right">
                                        <label class="text-right">
                                            <input type="checkbox" id="up-video">
                                            <span>اذا أردت رفع فيديو وصوره من جهازك الشخصي</span>
                                        </label>

                                        <div class="videoUploaded col-xs-12 text-right">
                                            <span><i class="fa fa-video-camera"></i> ارفع فيديو من جهازك</span>
                                            <input type="file" value="{{$course->course_video}}" name="course_video"
                                                   class="uploaded">
                                        </div>
                                        <div class="videoUploaded col-xs-12 text-right">
                                            <span><i class="fa fa-camera"></i> ارفع صوره من جهازك</span>
                                            <input type="file" value="{{$course->course_image}}" name="course_image"
                                                   class="uploaded">
                                        </div>

                                    </div>

                                </div>
                                <!-- /.lecture-item -->
                            </div>
                            <input type="text" value="{{$course->course_video_url}}" name="course_video_url"
                                   class="linked">

                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>وصف الدورة</h1>
                            <textarea name="course_description"
                                      placeholder="اضف وصف الدورة">{{$course->course_description}}</textarea>
                        </div>


                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>الجنس المتوقع</h1>
                            <div class="add_cont text-right">
                                <label class="text-right">

                                    <input type="checkbox" value="1" name="gender"
                                           @if($course->gender == 1) checked @endif>
                                    <span>ذكور</span>
                                </label>
                                <label class="text-right">
                                    <input type="checkbox" value="2" name="gender"
                                           @if($course->gender == 2) checked @endif>
                                    <span>إناث</span>
                                </label>
                            </div>
                        </div>

                        <div class="up_form-item">
                            <h1>نوع الدورة</h1>
                            <div class="add_cont text-right">
                                <label class="text-right">
                                    <input type="radio" value="1" id="rd_1" onclick="javascript:paymentMethod()"
                                           name="course_payment_method"
                                           @if($course->course_payment_method == 1 ) checked @endif >
                                    <span>مجاني</span>
                                </label>
                                <label class="text-right">
                                    <input type="radio" value="2" id="rd_2" onclick="javascript:paymentMethod()"
                                           name="course_payment_method"
                                           @if($course->course_payment_method == 2 ) checked @endif >
                                    <span>مدفوع</span>
                                </label>
                                <input type="number" name="course_salary" value="{{$course->course_salary}}"
                                       data-toggle="tooltip" id="c_salary" data-placement="top"
                                       title="اضف سعر الدورة">
                            </div>
                        </div>
                        <!-- /.up_form-item -->

                        <div class="up_form-item">
                            <a class="add-cert">اضافة شهادة للدورة</a>

                            <div class="course-cert">
                                <div class="up_form-item">
                                    <h1>إسم الشهادة</h1>
                                    <input type="text" name="certificate_name"
                                           value="{{$certificate->certificate_name}}">
                                </div>
                                <!-- /.up_form-item -->
                                <div class="up_form-item">
                                    <h1>الجهة المانحة</h1>
                                    <input type="text" name="certificate_branch"
                                           value="{{$certificate->certificate_branch}}">
                                </div>
                                <!-- /.up_form-item -->
                                <div class="up_form-item">
                                    <h1>تكلفة الشهادة</h1>
                                    <div class="add_cont text-right">
                                        <label class="text-right">
                                            <input type="radio" value="1" name="certificate_payment" id="cd_1"
                                                   onclick="javascript:certMethod()">
                                            <span>مجاني</span>
                                        </label>
                                        <label class="text-right">
                                            <input type="radio" value="2" name="certificate_payment" id="cd_2"
                                                   onclick="javascript:certMethod()">
                                            <span>مدفوع</span>
                                        </label>
                                        <input type="number" value="{{$certificate->certificate_salary}}"
                                               name="certificate_salary" id="cer_salary" data-toggle="tooltip"
                                               data-placement="top"
                                               title="اضف سعر الشهادة"></div>
                                </div>
                                <!-- /.up_form-item -->
                            </div>
                            <!-- /.course-cert -->
                        </div>
                        <!-- /.up_form-item -->

                        <div class="up_form-item up-confirm">
                            <input type="submit" value="تعديل الدورة">
                        </div>
                        <!-- /.up_form-item -->
                    </form>

                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>


@stop

<script type="text/javascript">

    function paymentMethod() {
        if (document.getElementById('rd_2').checked) {
            document.getElementById('c_salary').style.visibility = 'visible';
        }
        else {
            document.getElementById('c_salary').style.visibility = 'hidden';
        }
    }

    function certMethod() {
        if (document.getElementById('cd_2').checked) {
            document.getElementById('cer_salary').style.visibility = 'visible';
        }
        else {
            document.getElementById('cer_salary').style.visibility = 'hidden';
        }
    }

</script>