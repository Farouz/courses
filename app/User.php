<?php

namespace App;

use App\Notifications\VerfiyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fullName', 'email', 'password', 'phone', 'userName', 'country', 'gender', 'terms', 'is_teacher', 'is_trainee', 'job_title_', 'qualification', 'spcialization', 'Image'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token',];

    public function verified()
    {
        return $this->token === null;
    }

    public function sendVerificationEmail()
    {
        $this->notify(new VerfiyEmail($this));

    }

    public function Courses()
    {
        return $this->belongsToMany(Course::class, 'courses_users', 'user_id', 'course_id');
    }

    public function Lectures()
    {
        return $this->belongsToMany(User::class, 'lectures_users', 'user_id', 'lecture_id');
    }

    public function Notes()
    {
        return $this->hasMany(Note::class, 'user_id');
    }

    public function Discusses()
    {
        return $this->hasMany(Discuss::class, 'user_id');
    }

    public function Cv()
    {
        return $this->hasMany(CV::class, 'user_id');
    }
//    public function Interests(){
//        return $this->belongsToMany(Interest::class,'interests_users','user_id','interest_id');
//    }

    public function Certificates()
    {
        return $this->hasMany(Certificate::class, 'user_id');
    }
}

