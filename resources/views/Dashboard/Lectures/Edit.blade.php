@extends('Dashboard.layout.master')
@section('CssFiles')

    <link href="{{asset('User/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/selectric.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/reset.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/images/favicon.png')}}" rel="icon" type="text/css">
@stop
@section('content')

    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>تعديل محاضره
                    <br>
                    {{$lecture->lecture_title}}

                </h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">

                    <div class="add_lecture in-one">
                        <form action="{{route('POST_EDIT_LECTURE_DASH',$lecture->id)}}" method="post"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="lecture-item">
                                <h1>اسم المحاضره </h1>
                                <input type="text" name="lecture_title" value="{{$lecture->lecture_title}}">
                            </div>
                            <!-- /.lecture-item -->
                            <div class="up_form-item">
                                <h1>الدوره</h1>
                                <select name="course_id">
                                    <option value="{{\App\Course::where('id',$lecture->course_id)->first()->id}}">{{\App\Course::where('id',$lecture->course_id)->first()->course_title}}</option>
                                    @foreach($courses as $course)
                                        <option value="{{$course->id}}">{{$course->course_title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="up_form-item">
                                <h1>المدرس</h1>
                                <select name="teacher_id">
                                    <option value="{{\App\User::where('id',$lecture->teacher_id)->first()->id}}">{{\App\User::where('id',$lecture->teacher_id)->first()->fullName}}
                                        as {{\App\User::where('id',$lecture->teacher_id)->first()->userName}}</option>
                                    @foreach($teachers as $teacher)
                                        <option value="{{$teacher->id}}">{{$teacher->fullName}}
                                            as {{$teacher->userName}} </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="lecture-item">
                                <h1>اضف رابط خارجي للفيديو</h1>
                                <div class="add_cont text-right">
                                    <label class="text-right">
                                        <input type="checkbox" id="up-video">
                                        <span>اذا أردت رفع فيديو من جهازك الشخصي</span>
                                    </label>

                                    <div class="videoUploaded col-xs-12 text-right">
                                        <span><i class="fa fa-video-camera"></i> ارفع فيديو من جهازك</span>
                                        <input type="file" name="lecture_video" class="uploaded"
                                               value="{{$lecure->lecture_video}}">
                                    </div>
                                </div>
                                <input type="text" name="lecture_url" value="{{$lecture->lecture_url}}" class="linked">
                                <small>just Enter Youtube url instead if embeded</small>
                            </div>
                            <div class="lecture-item">
                                <h1>مده المحاضره </h1>
                                <input type="number" name="lecture_time" class="form-control"
                                       value="{{$lecture->lecture_time}}">
                            </div>
                            <div class="lecture-item">
                                <h1>ترتيب المحاضره </h1>
                                <input type="number" name="order" class="form-control" value="{{$lecture->order}}">
                            </div>
                            <!-- /.lecture-item -->
                            <div class="lecture-item">
                                <h1>وصف المحاضره </h1>
                                <textarea name="lecture_description"
                                          placeholder="اضف وصف المحاضرة">{{$lecture->lecture_description}}</textarea>
                            </div>
                            <!-- /.lecture-item -->
                            <div class="lecture-item text-right">
                                <div class="fileUpload col-xs-12 text-right">
                                    <span><i class="fa fa-file"></i> رابط أوراق العمل </span>
                                    <input type="file" name="lecture_file" class="upload"
                                           value="{{$lecture->lecture_file}}">
                                </div>
                                <span class="hint"> Image او Word او Powerpoint او Pdf الملفات يمكن ان تكون </span>
                            </div>
                            <!-- /.lecture-item -->
                            <div class="lecture-item confirm-lec">
                                <input type="submit" value="إضافة محاضرة">
                            </div>
                            <!-- /.lecture-item -->

                        </form>
                    </div>
                    <!-- /.add_lecture -->
                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>

@stop

<script src="{{asset('User/js/jquery-2.2.2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/html5shiv.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery-smoothscroll.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/modernizr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/placeholdem.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/toucheffects.js')}}"></script>
<script src="{{asset('User/js/jquery.selectric.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/classie.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery.nicescroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/script.js')}}" type="text/javascript"></script>