@extends('User.layout.master')
@section('Title')
    Sign Up
    @stop
@section('content')

    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>يرجي تسجيل حساب جديد</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">

                    <form action="{{route('POST_SIGNUP')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="up_form-item">
                            <span id="error-form">من فضلك ادخل البيانات الصحيحة</span>
                            <input type="text" name="fullName" placeholder="الإسم بالكامل" >
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="text" name="userName" placeholder="إسم المستخدم" >
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="text" name="email" placeholder="البريد الإلكتروني" >
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="password" name="password" placeholder="كلمة المرور" >
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="password" name="password_confirmation" placeholder="كلمة المرور" >
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="number" name="phone" placeholder="رقم الهاتف" >
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <select name="country">
                                <option>الدولة ...</option>
                                @foreach($countries as $country)
                                <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <select name="gender">
                                <option>الجنس ...</option>
                                <option value="1">مذكر</option>
                                <option value="2">مؤنث</option>
                            </select>
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item text-right">
                            <label>
                                <input name="is_teacher" value="1" type="checkbox">
                                <span>مدرب</span>
                                <a href="trainer-privacy.html" class="show-privacy">تعرف علي سياسة الخصوصية كمدرب</a>
                            </label>
                            <label>
                                <input  name="is_trainee" value="0" type="checkbox">
                                <span>متدرب</span>
                                <a href="trainer-privacy.html" class="show-privacy">تعرف علي سياسة الخصوصية كمتدرب</a>
                            </label>
                        </div>
                        <!-- /.up_form-item -->



                        <div class="policy text-right">
                            <label class="text-right policy">
                                <input type="checkbox" name="terms" required>
                                <span>هل انت موافق علي سياسة الخصوصية</span>
                            </label>
                        </div>
                        <!-- /.policy -->

                        <div class="up_form-item up-confirm">
                            <input type="submit" value="تسجيل">
                        </div>
                        <!-- /.up_form-item -->
                    </form>

                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>
    <!-- /.up-container -->
    @stop