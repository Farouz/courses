<?php

namespace App\Http\Controllers;

use App\Course;
use App\Mail\MailToAll;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MailsController extends Controller
{
    public function postSendMessages($id, Request $request,Mailer $mailer)
    {
//        $this->validate($request,[
//           'message'=>'required'
//        ]);
        $course = Course::find($id);
//        $message=$request->message;

        $users_id = DB::table('courses_users')->where('course_id', $course->id)->pluck('id');
        $users_emails=User::whereIn('id',$users_id)->pluck('email');
        $msg = "Message Test";
        $mailer->to($users_emails)->send(new MailToAll($users_emails,$msg));


        return redirect()->back()->with('success', 'Messages Sent');
    }
}
