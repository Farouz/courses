<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailToAll extends Mailable
{
    use Queueable, SerializesModels;
    public $this_email;
    public $msg;

    public function __construct($this_email, $msg)
    {
        $this->this_email = $this_email;
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('hossamfarouz15@gmail.com')->view('User.emailToAll');
    }
}
