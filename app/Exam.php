<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public function Answers(){
        return $this->hasMany(Answer::class,'exam_id');
    }
    protected $fillable=[
      'exam_title','course_id'
    ];
    public function Course(){
        return $this->belongsTo(Course::class,'course_id');
    }
}
