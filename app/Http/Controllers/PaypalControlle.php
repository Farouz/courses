<?php

namespace App\Http\Controllers;

use Netshell\Paypal\Facades\Paypal;
use Redirect;
use Illuminate\Http\Request;

class PaypalControlle extends Controller
{
    private $_apiContext;

    public function __construct()
    {
        $this->_apiContext = PayPal::ApiContext(config('paypal.client_id'), config('paypal.secret'));

        $this->_apiContext->setConfig(array('mode' => config('paypal.mode'), 'service.EndPoint' => config('paypal.link_mode'), 'http.ConnectionTimeOut' => config('paypal.timeout'), 'log.LogEnabled' => config('paypal.enable_log'), 'log.FileName' => storage_path('logs/paypal.log'), 'log.LogLevel' => 'FINE'));

    }

    public function getCheckout($currency,$course_title,$course_desc,$course_salary)
    {
        $payer = PayPal::Payer();
        $payer->setPaymentMethod('paypal');

        $amount = PayPal:: Amount();
        $amount->setCurrency('EUR');
        $amount->setTotal($course_salary); // This is the simple way,
        // you can alternatively describe everything in the order separately;
        // Reference the PayPal PHP REST SDK for details.

        $transaction = PayPal::Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription($course_desc);

        $redirectUrls = PayPal::RedirectUrls();
        $redirectUrls->setReturnUrl(route('GET_DONE'));
        $redirectUrls->setCancelUrl(route('GET_CANCEL'));

        $payment = PayPal::Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        $response = $payment->create($this->_apiContext);
        $redirectUrl = $response->links[1]->href;

        return Redirect::to( $redirectUrl );
    }
    public function check_payment($id,$token,$payer_id)
    {
        $payment = PayPal::getById($id, $this->_apiContext);
        $paymentExecution = PayPal::PaymentExecution();

        $paymentExecution->setPayerId($payer_id);
        return $payment->execute($paymentExecution, $this->_apiContext);

        // Clear the shopping cart, write to database, send notifications, etc.

        // Thank the user for the purchase
       // return view('checkout.done');

    }
}
