<?php

namespace App\Http\Controllers;

use App\CourseCategory;
use Illuminate\Http\Request;

class CourseCategoryController extends Controller
{
    public function getAddCategory()
    {
        return view('Dashboard.CourseCategory.addCategory');
    }

    public function postAddCategory(Request $request)
    {
        $this->validate($request, ['title_en' => 'required', 'title_ar' => 'required',]);
        $category = new CourseCategory();
        $category->title_en = $request->title_en;
        $category->title_ar = $request->title_ar;
        $category->save();
        return redirect()->route('GET_ALL_CATEGORIES')->with('success', 'Category Add ');
    }

    public function getAllCategory()
    {
        $categories = CourseCategory::all();
        return view('Dashboard.CourseCategory.AllCategories', compact('categories'));
    }

    public function getDeleteCategory($id)
    {
        CourseCategory::destroy($id);
        return redirect()->back()->with('alert', 'Course Category Deleted');
    }

    public function getEditCategory($id)
    {
        $category = CourseCategory::find($id);
        return view('Dashboard.CourseCategory.EditCategory', compact('category'));
    }

    public function postEditCategory($id, Request $request)
    {
        $this->validate($request, ['title_en' => 'required', 'title_ar' => 'required',]);
        $cat = CourseCategory::findOrFail($id);
        $cat->title_en = $request->title_en;
        $cat->title_ar = $request->title_ar;
        $cat->save();
        return redirect()->route('GET_ALL_CATEGORIES')->with('success', 'Category Updated');
    }
}
