<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullName');
            $table->string('userName')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('phone');
            $table->string('country');
            $table->string('gender');
            // 1 = teacher  0 = trainee
            $table->tinyInteger('is_teacher')->default(0);
            $table->tinyInteger('active')->default(0);
            $table->string('token')->nullable();
            $table->tinyInteger('terms');
            $table->rememberToken();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
