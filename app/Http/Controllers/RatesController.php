<?php

namespace App\Http\Controllers;

use App\Course;
use App\Fav;
use App\Rates;
use Illuminate\Http\Request;
use App\Setting;
use Illuminate\Support\Facades\Session;

class RatesController extends Controller
{
    public function addRateOne($id)
    {
        $course = Course::findOrfail($id);
        if (!Rates::where('user_id', \Auth::id())->exists()) {
            $rate = new Rates();
            $rate->user_id = \Auth::id();
            $rate->course_id = $course->id;
            $rate->rate = 1;
            return back()->with('success', 'Done');
        } else {
            Rates::where('user_id', \Auth::id())->update(['rate' => 1]);
            return back()->with('success', 'Done');
        }
    }

    public function addRateTwo($id)
    {
        $course = Course::findOrfail($id);
        if (!Rates::where('user_id', \Auth::id())->exists()) {
            $rate = new Rates();
            $rate->user_id = \Auth::id();
            $rate->course_id = $course->id;
            $rate->rate = 2;
            return back()->with('success', 'Done');
        } else {
            Rates::where('user_id', \Auth::id())->update(['rate' => 2]);
            return back()->with('success', 'Done');
        }
    }

    public function addRateThree($id)
    {
        $course = Course::findOrfail($id);
        if (!Rates::where('user_id', \Auth::id())->exists()) {
            $rate = new Rates();
            $rate->user_id = \Auth::id();
            $rate->course_id = $course->id;
            $rate->rate = 3;
            return back()->with('success', 'Done');
        } else {
            Rates::where('user_id', \Auth::id())->update(['rate' => 3]);
            return back()->with('success', 'Done');
        }
    }

    public function addRateFour($id)
    {
        $course = Course::findOrfail($id);
        if (!Rates::where('user_id', \Auth::id())->exists()) {
            $rate = new Rates();
            $rate->user_id = \Auth::id();
            $rate->course_id = $course->id;
            $rate->rate = 4;
            return back()->with('success', 'Done');
        } else {
            Rates::where('user_id', \Auth::id())->update(['rate' => 4]);
            return back()->with('success', 'Done');
        }
    }

    public function addRateFive($id)
    {
        $course = Course::findOrfail($id);
        if (!Rates::where('user_id', \Auth::id())->exists()) {
            $rate = new Rates();
            $rate->user_id = \Auth::id();
            $rate->course_id = $course->id;
            $rate->rate = 5;
            $rate->save();
            return back()->with('success', 'Done');
        } else {
            Rates::where('user_id', \Auth::id())->update(['rate' => 5]);
            return back()->with('success', 'Done');
        }
    }

//    public function RemoveFav($id)
//    {
//        $course = Course::find($id);
//        if (!Fav::where('user_id', \Auth::id())->exists()) {
//            $item=\Session::get('variable');
//            Fav::destroy($item->id);
//            return back()->with('success', 'Removed From Favourites');
//        } else {
//            return back()->with('success', 'Done');
//        }
//    }
//
//    public function addToFav($id)
//    {
//        $course = Course::find($id);
//        if (!Fav::where('user_id', \Auth::id())->exists()) {
//            $fav = new Fav();
//            $fav->user_id = \Auth::id();
//            $fav->course_id = $course->id;
//            $fav->save();
//            return back()->with('success', 'Added To Favourites');
//        } else {
//            return back()->with('success', 'Coruse Already In Favourites');
//        }
//
//    }
}
