@extends('User.layout.master')
@section('Title')
    {{$lecture->lecture_title}}
@stop

@section('content')

    <div class="intro-container">
        <div class="intro-head text-center">
            <div class="container">
                <h1>{{$lecture->lecture_title}}ا</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.intro-head -->
        <div class="corse-indv">
            <div class="container">
                <div class="mob-episodes col-xs-12">
                    <ul>
                        <li>
                            <h1>
                                <i class="fa fa-tasks"></i>
                                الاسبوع الاول
                            </h1>
                        </li>
                        <li>
                            <a href="#" class="active">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-play-circle"></i> الدرس الاول
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- end mob-episodes -->
                <div class="corse_indv-video col-md-12 col-xs-12 text-center">
                    <div class="corse_indv-inner">

                        <?php
                        if (!$lecture->lecture_url == null) {
                            $video = Embed::make($lecture->lecture_url)->parseUrl();
                            if ($video) {
                                $video->setAttribute(['width' => 700]);
                                echo $video->getHtml();
                            }
                        } else {
                            $video = Embed::make($lecture->lecture_video)->parseUrl();
                            if ($video) {
                                $video->setAttribute(['width' => 300]);
                                echo $video->getHtml();
                            }
                        }

                        ?>
                        <div class="finish-corse text-left col-xs-12">
                            <a href="{{route('GET_FINISH_COURSE',$lecture->id)}}">
                                لقد انهيت هذا الدرس
                            </a>
                            <?php
                                $course=App\Course::where('id',$lecture->course_id)->first();

                                $allLectures=\App\Lecture::where('course_id',$course->id)->count();

                                $user_lectures=\Illuminate\Support\Facades\DB::table('lectures_users')->select('lectures_id')->where('user_id',\Illuminate\Support\Facades\Auth::id())->count();

                                    ?>

                            @if($user_lectures == $allLectures)

                            <a href="{{route('GET_THE_EXAM',$course->id)}}">
                               اظهار الاختبار
                            </a>

                            @endif

                            @if(\Auth::id()==$course->teacher_id)
                                <a href="{{route('GET_ADD_EXAM',$course->id)}}">
                                    اضافه الاختبار
                                </a>
                                @endif
                            <div class="lesson-desc col-xs-12 text-right">
                                <h1>وصف المحاضرة</h1>
                                <p>
                                    {{$lecture->lecture_description}}
                                </p>
                            </div>
                        </div>
                        <!-- end finish-corse -->
                    </div>
                    <!-- end corse_indv-inner -->
                </div>
                <!-- end corse_indv-video -->

                <div class="corse-episodes col-md-3 col-xs-12 text-right">
<div class="corse_episodes-inner">
    <ul>
        <li>
            <h1>
                                    <i class="fa fa-tasks"></i>
                                    المحاضرات
                                </h1>
        </li>
        @foreach($lectures as $item)
        <li>
            <a href="{{route('GET_SINGLE_LECTURE',$item->id)}}" class="active">
                <i class="fa fa-play-circle"></i> {{$item->lecture_title}}
            </a>
        </li>
            @endforeach
    </ul>
</div>
</div>

                <div class="corse-comments col-xs-12">
                    <div class="disqus-comments">
                        <div class="empty-msg text-center animated shake">
                            <h1>
                                <i class="fa fa-commenting-o"></i>
                                عفوا لا توجد تعليقات علي هذا الدرس </h1>
                        </div>
                    </div>
                    <!-- end disqus-comments -->
                </div>
                <!-- end corse-comments -->
            </div>
            <!-- end container -->
        </div>
        <!-- end corse-indv -->
    </div>


@stop
