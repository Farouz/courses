<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['first_answer', 'exam_id', 'second_answer', 'third_answer', 'forth_answer'];

    public function Exam()
    {
        return $this->belongsTo(Answer::class, 'exam_id');
    }
}
