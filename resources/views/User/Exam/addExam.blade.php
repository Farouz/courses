@extends('User.layout.master')
@section('Title')
    أضافه اختبار
@stop

@section('content')
    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>إضافة اختبار جديد</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">

                    <div class="add_lecture in-one">
                        <form action="{{route('POST_ADD_EXAM')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="course_id" value="{{$course->id}}">
                            <div class="lecture-item">
                                <h1>اضف السؤال</h1>
                                <textarea name="exam_title" placeholder="اكتب سؤالك هنا"></textarea>
                            </div>
                            <!-- end lecture-item -->

                            <div class="lecture-item">
                                <h1>يجب عليك اختيار اجابة صحيحة واحده</h1>
                                <ul>
                                    <li>
                                        <input type="checkbox" name="correct" value="1">
                                        <input type="text" name="first_answer" placeholder="اكتب الاجابة الاولي">
                                    </li>
                                    <li>
                                        <input type="checkbox" name="correct" value="2">
                                        <input type="text" name="second_answer" placeholder="اكتب الاجابة الثانية">
                                    </li>
                                    <li>
                                        <input type="checkbox" name="correct" value="3">
                                        <input type="text" name="third_answer" placeholder="اكتب الاجابة الثالثة">
                                    </li>
                                    <li>
                                        <input type="checkbox" name="correct" value="4">
                                        <input type="text" name="forth_answer" placeholder="اكتب الاجابة الاخيرة">
                                    </li>
                                </ul>
                            </div>
                            <div class="lecture-item confirm-lec">
                                <input type="submit" value="نشر الاختبار">
                            </div>
                            <!-- end lecture-item -->
                        </form>
                    </div>
                    <!-- /.add_lecture -->
                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>
@stop
@section('UserScripts')
    <script src="{{asset('User/js/bootstrap-checkbox.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(':checkbox').checkboxpicker({
            onLabel: 'Right',
            offLabel: 'Wrong'
        });
    </script>
@stop