<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course_title');
            $table->string('pre_exp');
            $table->string('course_category');
            $table->string('course_video');
            $table->string('course_video_url');
            $table->text('course_description');
            $table->tinyInteger('gender');
            $table->tinyInteger('course_payment_method');
            $table->decimal('course_salary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
