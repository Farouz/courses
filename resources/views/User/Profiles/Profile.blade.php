<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 ie-all" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 9]>
<html class="ie9 ie-all" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 10]>
<html class="ie10 ie-all" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if !IE]><!-->
<!--<![endif]-->
<html>

<head>
    <title> Profile </title>
    <meta name="author" content="Hossam Farouz">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <meta charset="utf-8">

    <!-- Css Files -->
    <link href="{{asset('User/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/selectric.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/reset.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/images/favicon.png')}}" rel="icon" type="text/css">
</head>

<body>

<!-- start the loading screen -->
<div class="wrap">
    <div class="loading">
        <div class="bounceball"></div>
        <div class="text">NOW LOADING</div>
    </div>
</div>

<!-- end the loading screen -->

<div class="wrapper">
    <header>
        <div class="dividers">
            <span class="t1"></span>
            <span class="t2"></span>
            <span class="t3"></span>
            <span class="t4"></span>
            <span class="t5"></span>
            <span class="t1"></span>
            <span class="t2"></span>
            <span class="t3"></span>
            <span class="t4"></span>
            <span class="t5"></span>
        </div>
        <!-- /.dividers -->

        <div class="header-nav">
            <div class="container">
                <div class="nav-right user_nav-right col-md-6 col-xs-12 pull-right">

                    <div class="logo">
                        <a href="{{route('home')}}" title="العلوم العصرية للتدريب">
                            <img src="{{asset('User/images/logo.png')}}" alt="site-logo" width="110" height="70">
                        </a>
                    </div>
                    <!-- /.logo -->
                </div>
                <!-- /.nav-logo -->
                <div class="nav-left user_nav-left col-md-6 col-xs-12 pull-left">
                    <div class="user-logged">
                        <ul>
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" class="hvr-underline-reveal">
                                        <span class="cont-img">
                                    <img src="{{asset('User/images/'.$user->Image)}}" width="35" height="35"
                                         alt="User-Img">
                                </span>
                                    <b>{{$user->userName}}</b>
                                    <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                                    <div class="drop drop-links col-xs-12">
                                        <div class="drop-links">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user"></i>&nbsp; حسابي
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('LOG_OUT')}}">
                                                        <i class="fa fa-power-off"></i>&nbsp; خروج
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- end drop-links -->
                                    </div>
                                    <!-- end drop -->
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="show-user_search">
                                    <i class="fa fa-search"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="show-notification" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell"></i>
                                </a>
                                <ul class="dropdown-menu notification-box" role="menu" aria-labelledby="dropdownMenu">
                                    <div class="drop drop-links col-xs-12">
                                        @foreach($notes as $note)
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('User/images/'.App\User::where('id',$note->user_id)->first()->Image)}}"
                                                             alt=""
                                                             class="img-circle pull-right">
                                                        <h4>
                                                            {{\App\User::where('id',$note->user_id)->first()->fullName}}
                                                            <small>
                                                                <i class="fa fa-clock-o"></i>{{$note->created_at->diffForHumans()}}
                                                            </small>
                                                        </h4>
                                                        <p>
                                                            {{$note->note_description}}
                                                        </p>
                                                    </a>
                                                </li>

                                            </ul>
                                        @endforeach
                                        @foreach($discuss as $dis)


                                            <ul>

                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('User/images/'.App\User::where('id',$dis->user_id)->first()->Image)}}"
                                                             alt=""
                                                             class="img-circle pull-right">
                                                        <h4>
                                                            {{App\User::where('id',$dis->user_id)->first()->fullName}}
                                                            <small><i class="fa fa-clock-o"></i>
                                                                {{$dis->created_at->diffForHumans()}}
                                                            </small>
                                                        </h4>
                                                        <p>
                                                            {{$dis->discuss_body}}
                                                        </p>
                                                    </a>
                                                </li>

                                            </ul>


                                        @endforeach
                                    </div>
                                    <!-- end drop -->
                                </ul>
                            </li>

                        </ul>
                    </div>
                    <!-- /.user-controls -->
                </div>
                <!-- /.nav-user -->

            </div>
            <!-- /.container -->
            <div class="input-container user-search col-md-12 col-xs-12 input-lft">
                <div class="container">
                    <form method="post" action="{{route('SEARCH_FOR_COURSES')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="text" name="course_search" placeholder="ابحث عن جميع الكورسات من هنا">
                        <button type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </form>

                </div>
                <!-- /.container -->
            </div>
            <!-- /.input-container -->
        </div>
        <!-- /.header-nav -->

    </header>
    <!-- /header -->

    <div class="profile-content empty-course">
        <div class="container">
            <div class="right_tap-box col-md-3 col-xs-12 hidden-xs hidden-sm pull-right">
                <div class="right_box-inner">
                    <!-- Nav tabs -->
                    <a class="toggle-slidenav hidden-xs hidden-sm">
                        <i class="fa fa-close"></i>
                    </a>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">

                            <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                <i class="fa fa-user"></i> الملف الشخصي
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#password" aria-controls="password" role="tab" data-toggle="tab">
                                <i class="fa fa-lock"></i> كلمة المرور
                            </a>
                        </li>
                        @if(\Auth::user()->is_teacher ==1)
                            <li role="presentation">
                                <a href="#courses" aria-controls="courses" role="tab" data-toggle="tab">
                                    <i class="fa fa-database"></i> الدورات
                                </a>
                            </li>
                        @endif
                        <li role="presentation">
                            <a href="#interests" aria-controls="interests" role="tab" data-toggle="tab">
                                <i class="fa fa-diamond"></i> الاهتمامات
                            </a>
                        </li>
                        @if(\Auth::user()->is_teacher == 1)

                            <li role="presentation">
                                <a href="#cv" aria-controls="cv" role="tab" data-toggle="tab">
                                    <i class="fa fa-file-text-o"></i> السيرة الذاتية
                                </a>
                            </li>
                        @endif
                        <li role="presentation">
                            <a href="#all-courses" aria-controls="all-courses" role="tab" data-toggle="tab">
                                <i class="fa fa-eye"></i> تصفح الدورات
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#my_courses" aria-controls="my_courses" role="tab" data-toggle="tab">
                                <i class="fa fa-folder-open-o"></i> دوراتي كمتدرب
                            </a>
                        </li>

                        <li role="presentation">
                            <a href="#my_certf" aria-controls="my_certf" role="tab" data-toggle="tab">
                                <i class="fa fa-table"></i> شهاداتي كمتدرب
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- /.right_box-inner -->
            </div>
            <!-- /.right_tap-box -->

            <div class="mobile_tap-box col-md-12 col-xs-12 hidden-lg text-center">
                <div class="right_box-inner">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">

                            <a href="#home" aria-controls="home" role="tab" data-toggle="tab" title="الملف الشخصي">
                                <i class="fa fa-user"></i>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#password" aria-controls="password" role="tab" data-toggle="tab"
                               title="كلمة المرور">
                                <i class="fa fa-lock"></i>
                            </a>
                        </li>
                        @if(Auth::user()->is_teacher)
                            <li role="presentation">
                                <a href="#courses" aria-controls="courses" role="tab" data-toggle="tab" title="الدورات">
                                    <i class="fa fa-database"></i>
                                </a>
                            </li>
                        @endif

                        <li role="presentation">
                            <a href="#interests" aria-controls="interests" role="tab" data-toggle="tab"
                               title="الاهتمامات">
                                <i class="fa fa-diamond"></i>
                            </a>
                        </li>
                        @if(Auth::user()->is_teacher)

                            <li role="presentation">
                                <a href="#cv" aria-controls="cv" role="tab" data-toggle="tab" title="السيرة الذاتية">
                                    <i class="fa fa-file-text-o"></i>
                                </a>
                            </li>
                        @endif
                        <li role="presentation">
                            <a href="#all-courses" aria-controls="all-courses" role="tab" data-toggle="tab"
                               title="تصفح الدورات">
                                <i class="fa fa-eye"></i>
                            </a>
                        </li>

                        <li role="presentation">
                            <a href="#my_courses" aria-controls="my_courses" role="tab" data-toggle="tab"
                               title="دوراتي كمتدرب">
                                <i class="fa fa-folder-open-o"></i>
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- /.right_box-inner -->
            </div>
            <!-- /.mobile_tap-box -->

            <div class="left_tap-box col-md-9 col-xs-12 pull-left">
                <div class="left_box-inner">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in  active" id="home">
                            <div class="home-head">
                                <h1>
                                    <i class="fa fa-user"></i>
                                    الملف الشخصي
                                    <a class="edit-personal">
                                        <i class="fa fa-cog"></i>
                                        تعديل البيانات
                                    </a>
                                    <button class="cancel-personal" type="reset">
                                        <i class="fa fa-times"></i>
                                        إلغاء التعديل
                                    </button>
                                </h1>
                            </div>
                            <!-- /.home-head -->
                            <form method="post" action="{{route('POST_EDIT_PROFILE',$user->id)}}"
                                  enctype="multipart/form-data">


                                <div class="home_img  text-center">
                                    <div class="home_img-inner">
                                        <div class="left-caption col-xs-12">
                                            <div class="imgcontent col-xs-12">
                                                <div class="bstext">
                                                    <span>
                                          <i class="fa fa-camera"></i><br>
                                          Upload an image
                                      </span>
                                                </div>
                                                <!-- /.bstext -->
                                                {{--USER UPLOAD IMAGE--}}
                                                <output id="list"></output>
                                                <input type="file" id="show-adj8" name="Image">
                                            </div>
                                            <!-- /.imgcontent -->
                                        </div>
                                        <!-- /.left-caption -->
                                        {{--USER IMAGE --}}
                                        <img src="{{asset('User/images/'.$user->Image)}}" alt="" width="150"
                                             height="150">
                                    </div>
                                </div>
                                <!-- /.home_img -->
                                <div class="home-content">

                                    <div class="home_data col-md-10 col-sm-10 col-xs-12 text-right">
                                        <form action="#" method="get">
                                            <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                                                <div>
                                                    <i class="fa fa-user-secret"></i>
                                                    <h1>الإسم بالكامل</h1>
                                                    <input type="text" id="edit-area" name="fullName"
                                                           value="{{$user->fullName}}">
                                                    <span>{{$user->fullName}}</span>
                                                </div>
                                            </div>
                                            <!-- /.home_data-item -->

                                            <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                                                <div>
                                                    <i class="fa fa-user"></i>
                                                    <h1>إسم المستخدم</h1>
                                                    <input type="text" id="edit-area" name="userName"
                                                           value="{{$user->userName}}">
                                                    <span>{{$user->userName}}</span>
                                                </div>
                                            </div>
                                            <!-- /.home_data-item -->
                                            <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                                                <div>
                                                    <i class="fa fa-phone"></i>
                                                    <h1>رقم الهاتف</h1>
                                                    <input type="text" id="edit-area" name="phone"
                                                           value="{{$user->phone}}">
                                                    <span>{{$user->phone}}</span>
                                                </div>
                                            </div>
                                            <!-- /.home_data-item -->

                                            <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                                                <div>
                                                    <i class="fa fa-envelope"></i>
                                                    <h1>البريد الإلكتروني</h1>
                                                    <input type="email" name="email" value="{{$user->email}}"
                                                           id="edit-area">
                                                    <span>{{$user->email}}</span>
                                                </div>
                                            </div>
                                            <!-- /.home_data-item -->
                                            <div class="home_data-item col-md-6 col-sm-6  col-xs-12 pull-right">
                                                <div>
                                                    <i class="fa fa-globe"></i>
                                                    <h1>الدولة</h1>
                                                    <select name="country" id="edit-area">
                                                        @foreach($countries as $country)
                                                            <option>{{\App\Country::where('country_name',$user->country)->first()->country_name}}</option>
                                                            <option value="{{$country->country_code}}">{{$country->country_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span>{{$user->country}}</span>
                                                </div>
                                            </div>
                                            <!-- /.home_data-item -->
                                            <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                                                <div>
                                                    <i class="fa fa-male"></i>
                                                    <h1>الجنس</h1>
                                                    <select name="gender" id="edit-area">
                                                        <option value="1">مذكر</option>
                                                        <option value="2">مؤنث</option>
                                                    </select>
                                                    <?php
                                                    if ($user->gender == 1) {
                                                        echo '<span> مذكر </span>';
                                                    } else {
                                                        echo '<span>مؤنث </span>';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <!-- /.home_data-item -->
                                            <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                                                <div>
                                                    <i class="fa fa-globe"></i>
                                                    <h1>مدرب / متدرب</h1>
                                                    <select name="is_teacher" id="edit-area">
                                                        <option value="1">مدرب</option>
                                                        <option value="0">متدرب</option>
                                                    </select>
                                                    <?php
                                                    if ($user->is_teacher == 1) {
                                                        echo ' <span>مدرب</span>';
                                                    } else {
                                                        echo ' <span>متدرب</span>';

                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <!-- /.home_data-item -->

                                            <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                                                <div>
                                                    <i class="fa fa-graduation-cap"></i>
                                                    <h1> المؤهل</h1>
                                                    <input type="text" value="{{$user->qualification}}"
                                                           name="qualification" id="edit-area">
                                                    <span>{{$user->qualification}}</span>
                                                </div>
                                            </div>
                                            <!-- /.home_data-item -->

                                            <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                                                <div>
                                                    <i class="fa fa-briefcase"></i>
                                                    <h1>التخصص</h1>
                                                    <input type="text" value="{{$user->spcialization}}"
                                                           name="spcialization" id="edit-area">
                                                    <span>{{$user->spcialization}}</span>
                                                </div>
                                            </div>
                                            <!-- /.home_data-item -->

                                            <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                                                <div>
                                                    <i class="fa fa-cogs"></i>
                                                    <h1>مجال العمل</h1>
                                                    <input type="text" value="{{$user->job_title}}" name="job_title"
                                                           id="edit-area">
                                                    <span>{{$user->job_title}} </span>
                                                </div>
                                            </div>
                                            <!-- /.home_data-item -->

                                            <div class="home_data-item all-set col-md-12 col-sm-12  col-xs-12 pull-right">
                                                {{csrf_field()}}
                                                <input type="submit" class="confirm-set" value="حفظ التعديلات">
                                            </div>
                                            <!-- /.home_data-item -->
                                        </form>
                                        @include('User.layout.messages')
                                    </div>
                                    <!-- ./home_data -->
                                </div>
                                <!-- /.home-content -->
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="password">
                        {{--<div class="home-head">--}}
                        {{--<h1>--}}
                        {{--<i class="fa fa-lock"></i>--}}
                        {{--كلمة المرور--}}
                        {{--</h1>--}}
                        {{--<a class="edit-personal">--}}
                        {{--<i class="fa fa-cog"></i> تعديل البيانات--}}
                        {{--</a>--}}
                        {{--<button class="cancel-personal" type="reset">--}}
                        {{--<i class="fa fa-times"></i> إلغاء التعديل--}}
                        {{--</button>--}}
                        {{--</div>--}}
                        <!-- /.home-head -->
                            <div class="home-content pass-content col-xs-12">
                                <form action="{{route('POST_CHANGE_PASSWORD',Auth::id())}}" method="post"
                                      enctype="multipart/form-data">
                                    {{csrf_field()}}

                                    <div class="home_data col-xs-12 pull-right text-right">
                                        <div class="home_data-item all-pass col-md-12  col-xs-12 pull-right">
                                            <div>
                                                <i class="fa fa-lock"></i>
                                                <h1>كلمة المرور القديمة</h1>
                                                <input type="password" class="form-control" name="old_password" style="
                                            display: inline;
                                                    width: 60%;
                                                        height: 30px;
                                                        border: 1px solid #dfdfdf;
                                                        outline: 0;
                                                        line-height: 30px;
                                                        text-indent: 10px;
                                                        font-size: 13px;
                                                        position: absolute;
                                                        top: 2px;">

                                            </div>
                                        </div>
                                        <!-- /.home_data-item -->

                                        <div class="home_data-item all-pass col-md-12  col-xs-12 pull-right">
                                            <div>
                                                <i class="fa fa-unlock"></i>
                                                <h1>كلمة المرور الجديدة</h1>
                                                <input name="password" class="form-control" type="password" style="
                                            display: inline;
                                                    width: 60%;
                                                        height: 30px;
                                                        border: 1px solid #dfdfdf;
                                                        outline: 0;
                                                        line-height: 30px;
                                                        text-indent: 10px;
                                                        font-size: 13px;
                                                        position: absolute;
                                                        top: 2px;">>
                                            </div>
                                        </div>
                                        <!-- /.home_data-item -->

                                        <div class="home_data-item all-pass col-md-12  col-xs-12 pull-right">
                                            <div>
                                                <i class="fa fa-lock"></i>
                                                <h1>إعادة كتابة كلمة المرور الجديدة</h1>
                                                <input type="password" name="password_confirmation" class="form-control"
                                                       style="
                                            display: inline;
                                                    width: 60%;
                                                        height: 30px;
                                                        border: 1px solid #dfdfdf;
                                                        outline: 0;
                                                        line-height: 30px;
                                                        text-indent: 10px;
                                                        font-size: 13px;
                                                        position: absolute;
                                                        top: 2px;">>
                                            </div>
                                            <button type="submit" class="btn btn-success pull-left">حفظ التعديلات
                                            </button>
                                        </div>
                                        <!-- /.home_data-item -->
                                        <div class="home_data-item all-pass col-md-12 col-sm-12  col-xs-12 pull-right">
                                            <input type="submit" value="حفظ التعديلات" class="confirm-set">
                                        </div>
                                        <!-- /.home_data-item -->
                                    </div>
                                </form>

                                <!-- ./home_data -->
                            </div>
                            <!-- /.home-content -->
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="courses">
                            <div class="home-head">
                                <h1>
                                    <i class="fa fa-database"></i>
                                    الدورات
                                </h1>
                                @if((\Auth::user()->is_teacher)== 1)
                                    <a class="add1_course" href="{{route('GET_ADD_COURSE')}}">
                                        <i class="fa fa-plus"></i>إضافة دورة
                                    </a>
                                @endif
                            </div>
                            <!-- /.home-head -->
                            <div class="home-content  pass-content col-xs-12">
                                <div class="home_data col-md-12 pull-right text-right">
                                    <div class="shop-wrapper col-xs-12">

                                        @if((\Auth::user()->is_teacher)== 1)

                                            <div class="panel-group" id="accordion" role="tablist"
                                                 aria-multiselectable="true">
                                                @foreach($teacherCourses as $teacherCourse)
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="button" role="tab"
                                                             id="headingOne"
                                                             role="button" data-toggle="collapse"
                                                             data-parent="#accordion"
                                                             href="#collapseOne" aria-expanded="true"
                                                             aria-controls="collapseOne">
                                                            <h4 class="panel-title">
                                                                <a>
                                                                    <h5>
                                                                        <i class="fa fa-group"></i>

                                                                        <?php
                                                                        $students = \Illuminate\Support\Facades\DB::table('courses_users')->select('user_id')->where('course_id', $teacherCourse->id)->count();
                                                                        echo $students;
                                                                        ?>

                                                                    </h5>
                                                                    {{$teacherCourse->course_title}}</a>


                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in"
                                                             role="tabpanel"
                                                             aria-labelledby="headingOne">
                                                            <div class="panel-body">
                                                                <div class="instructor-control text-center">

                                                                    <a href="{{route('DELETE_THIS_COURSE',$teacherCourse->id)}}"
                                                                       class="delete-course">
                                                                        <i class="fa fa-trash"></i> حذف الدورة
                                                                    </a>
                                                                    <a href="#" class="add-course">
                                                                        <i class="fa fa-plus"></i> إضافة محاضرة
                                                                    </a>
                                                                    <a href="#" class="message-course">
                                                                        <i class="fa fa-envelope"></i> إرسال للجميع
                                                                    </a>
                                                                    <!-- =========================================================================================================================================== -->

                                                                    <div class="panel-pop modal" id="msg-all">
                                                                        <div class="lost-inner">
                                                                            <h1>
                                                                                <i class="fa fa-envelope"></i>
                                                                                إرسال لجميع الطلاب المشتركين في الدورة
                                                                            </h1>
                                                                            <div class="lost-item" id="messageTo">
                                                                        <textarea
                                                                                placeholder="اكتب الرسالة هنا"></textarea>
                                                                            </div>
                                                                            <!-- /.lost-item -->
                                                                            <div class="text-center">
                                                                                <input type="submit" value="إرسال">
                                                                            </div>
                                                                            <!-- /.lost-item -->
                                                                        </div>
                                                                        <!-- /.lost-inner -->
                                                                    </div>
                                                                    <!-- /.modal -->

                                                                    <!-- =========================================================================================================================================== -->
                                                                    <a href="{{route('GET_EDIT_COURSE',$teacherCourse->id)}}"
                                                                       class="edit-course">
                                                                        <i class="fa fa-pencil"></i> تعديل الدورة
                                                                    </a>
                                                                    <a href="#" class="add-alert-form">
                                                                        <i class="fa fa-bullhorn"></i> إضافة تنويه
                                                                    </a>

                                                                    <!-- =========================================================================================================================================== -->
                                                                    <form action="{{route('POST_SEND_NOTE')}}"
                                                                          method="post"
                                                                          enctype="multipart/form-data">
                                                                        {{csrf_field()}}
                                                                        <input type="hidden"
                                                                               value="{{\Auth::user()->id}}"
                                                                               name="user_id">
                                                                        <input type="hidden"
                                                                               value="{{$teacherCourse->id}}"
                                                                               name="course_id">
                                                                        <div class="panel-pop modal" id="alert-all">
                                                                            <div class="lost-inner">
                                                                                <h1>
                                                                                    <i class="fa fa-envelope"></i>
                                                                                    اضافة تنويه للطلاب المشتركين في
                                                                                    الدورة
                                                                                </h1>
                                                                                <div class="lost-item" id="alert-item">
                                                                                    <input type="text" name="note_title"
                                                                                           placeholder="عنوان التنويه">
                                                                                </div>
                                                                                <!-- /.lost-item -->
                                                                                <div class="lost-item" id="alert-item">
                                                                        <textarea name="note_description"
                                                                                  placeholder="مضمون التنويه"></textarea>
                                                                                </div>
                                                                                <!-- /.lost-item -->
                                                                                <div class="text-center">
                                                                                    <input type="submit"
                                                                                           value="نشر التنويه">
                                                                                </div>
                                                                                <!-- /.lost-item -->
                                                                            </div>
                                                                            <!-- /.lost-inner -->
                                                                        </div>
                                                                        <!-- /.modal -->
                                                                    </form>
                                                                    <!-- =========================================================================================================================================== -->
                                                                    <div class="add_lecture">
                                                                        <form action="{{route('POST_ADD_LECTURE')}}"
                                                                              method="post"
                                                                              enctype="multipart/form-data">
                                                                            {{csrf_field()}}
                                                                            <input type="hidden" name="course_id"
                                                                                   value="{{$teacherCourse->id}}">

                                                                            <div class="lecture-item">
                                                                                <h1>اسم الدرس</h1>
                                                                                <input type="text" name="lecture_title"
                                                                                       placeholder="اضف اسم المحاضرة">
                                                                            </div>
                                                                            <!-- /.lecture-item -->
                                                                            <div class="lecture-item">
                                                                                <h1>اضف رابط خارجي للفيديو</h1>
                                                                                <div class="add_cont text-right">
                                                                                    <label class="text-right">
                                                                                        <input type="checkbox"
                                                                                               id="up-video">
                                                                                        <span>اذا أردت رفع فيديو من جهازك الشخصي</span>
                                                                                    </label>

                                                                                    <div class="videoUploaded col-xs-12 text-right">
                                                                                        <span><i class="fa fa-video-camera"></i> ارفع فيديو من جهازك</span>
                                                                                        <input type="file"
                                                                                               name="lecture_video"
                                                                                               class="uploaded">
                                                                                    </div>
                                                                                    <!--
                                                                                    <label class="text-right">
                                                                                        <input type="radio" id="add-link">
                                                                                        <span>يوتيوب</span>
                                                                                    </label>
        -->
                                                                                </div>
                                                                                <input type="text" name="lecture_url"
                                                                                       placeholder="ادخل رابط فيديو"
                                                                                       class="linked">
                                                                            </div>
                                                                            <!-- /.lecture-item -->
                                                                            <div class="lecture-item">
                                                                                <h1>اسم الدرس</h1>
                                                                                <textarea name="lecture_description"
                                                                                          placeholder="اضف وصف المحاضرة"></textarea>
                                                                            </div>
                                                                            <!-- /.lecture-item -->
                                                                            <div class="lecture-item text-right">
                                                                                <div class="fileUpload col-xs-12 text-right">
                                                                                    <span><i class="fa fa-file"></i> رابط أوراق العمل </span>
                                                                                    <input type="file"
                                                                                           name="lecture_file"
                                                                                           class="upload">
                                                                                </div>
                                                                                <span class="hint"> Image او Word او Powerpoint او Pdf الملفات يمكن ان تكون </span>
                                                                            </div>
                                                                            <!-- /.lecture-item -->
                                                                            <div class="lecture-item add-sorting">
                                                                                <label>
                                                                                    <input type="checkbox"
                                                                                           id="sort-lesson">
                                                                                    <span>يجب تحديد ترتيب الدرس </span>
                                                                                    <input type="number"
                                                                                           name="count"
                                                                                           data-toggle="tooltip"
                                                                                           data-placement="top"
                                                                                           title="اكتب ترتيب الدرس بالأرقام"
                                                                                           class="add_sort-number">
                                                                                </label>
                                                                            </div>
                                                                            <!-- /.lecture-item -->
                                                                            <div class="lecture-item confirm-lec">
                                                                                <input type="submit"
                                                                                       value="إضافة محاضرة">
                                                                            </div>
                                                                            <!-- /.lecture-item -->

                                                                        </form>
                                                                    </div>
                                                                    <!-- /.add_lecture -->
                                                                </div>
                                                                <!-- /.instructor-control -->
                                                                <ul>
                                                                    <li>
                                                                        <h1>
                                                                            <label>الوصف</label>
                                                                            <span class="par">{{$teacherCourse->course_description}}</span>
                                                                        </h1>
                                                                    </li>
                                                                    <li>
                                                                        <h1>
                                                                            <label>المجال</label>
                                                                            <span>{{\App\CourseCategory::where('id',$teacherCourse->cat_id)->first()->title_en}}</span>
                                                                        </h1>
                                                                    </li>

                                                                    <li>
                                                                        <h1>
                                                                            <label>عدد المشتركين في الدورة</label>
                                                                            <span>

                                                                    <?php
                                                                                $students = \Illuminate\Support\Facades\DB::table('courses_users')->select('user_id')->where('course_id', $teacherCourse->id)->count();
                                                                                echo $students . ' طلاب ';
                                                                                ?>

                                                                        </span>
                                                                        </h1>
                                                                    </li>
                                                                    <li>
                                                                        <h1>
                                                                            <label>الحالة</label>
                                                                            <span>
                                                                                @if($teacherCourse->approved == 1)
                                                                                    تم الموافقه عليها
                                                                                @else
                                                                                    لم يتم الموافقه عليها بعد
                                                                                @endif
                                                                        </span>
                                                                        </h1>
                                                                    </li>
                                                                    @if(! $teacherCourse->certificate_name == null)
                                                                        <li>
                                                                            <h1>
                                                                                <label>الشهادة</label>
                                                                                <span>
{{$teacherCourse->certificate_name}}
                                                                            </span>
                                                                            </h1>
                                                                        </li>
                                                                    @endif
                                                                    @if(! $teacherCourse->certificate_salary== null)
                                                                        <li>

                                                                            <h1>
                                                                                <label>سعر الشهاده</label>
                                                                                <span>{{$teacherCourse->certificate_salary}}</span>
                                                                            </h1>
                                                                        </li>
                                                                    @endif

                                                                    <li>
                                                                        <h1>
                                                                            <label>التاريخ</label>
                                                                            <span>{{$teacherCourse->course_date}}</span>
                                                                        </h1>
                                                                    </li>
                                                                    <li>
                                                                        <h1>
                                                                            <label>سعر الدوره</label>
                                                                            @if($teacherCourse->course_salary==null)
                                                                                <span>مجاناً</span>
                                                                            @else
                                                                                <span>{{$teacherCourse->course_salary}}</span>
                                                                            @endif
                                                                        </h1>
                                                                    </li>
                                                                    <li>
                                                                        <h1>
                                                                            <label>إسم المدرب</label>
                                                                            <span>{{App\User::where('id',$teacherCourse->teacher_id)->first()->fullName}}</span>
                                                                        </h1>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <!-- /.panel-body -->

                                                        </div>
                                                        <!-- /#collapseOne -->
                                                    </div>
                                                    <!-- /.panel-default -->
                                                @endforeach
                                            </div>
                                            <!-- /.panel-group -->

                                        @endif
                                    </div>
                                    <!-- end shop-wrapper -->
                                </div>
                                <!-- ./home_data -->
                            </div>
                            <!-- /.home-content -->
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="cv">
                            <div class="home-head">
                                <h1>
                                    <i class="fa fa-file"></i>
                                    أضف ملف سيرتك الذاتية
                                </h1>
                            </div>
                            <!-- /.home-head -->
                            @if((\Auth::user()->is_teacher)== 1)

                                <div class="home-content pass-content col-xs-12">
                                    <div class="home_data col-xs-12 pull-right text-right">
                                        <div class="home_data-item col-md-12  col-xs-12 pull-right">
                                            <div>
                                                <form class="cv-file" method="post" action="{{route('POST_ADD_CV')}}">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="user_id" value="{{Auth::id()}}">
                                                    <h1>أضف رابط خارجي لملف السيرة الذاتية</h1>
                                                    <input type="url" name="cv_url" placeholder="رابط خارجي">
                                                    <h1>او يمكنك كتابتها بنفسك من خلال</h1>
                                                    <textarea name="cv_body"
                                                              placeholder="اكتب سيرتك الذاتية"></textarea>
                                                    <input type="submit" value="حفظ">

                                                </form>
                                            </div>


                                            <a class="show-cv">عرض ملف السيرة الذاتية</a>
                                            <div class="cv-container text-center">
                                                @if(! Auth::user()->cvs == null)
                                                    <p>
                                                        {{\App\CV::where('user_id',Auth::id())->first()->cv_body}}
                                                    </p>
                                                    <a href="{{\App\CV::where('user_id',Auth::id())->first()->cv_url}}">
                                                        <i class="fa fa-cloud-download"></i> تحميل ملف السيرة الذاتية
                                                    </a>
                                                @endif
                                            </div>
                                            <!-- /.cv-container -->
                                        </div>
                                        <!-- /.home_data-item -->


                                    </div>
                                    <!-- ./home_data -->
                                </div>
                        @endif
                        <!-- /.home-content -->
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="interests">
                            <div class="home-head">
                                <h1>
                                    <i class="fa fa-diamond"></i>
                                    الاهتمامات
                                </h1>
                            </div>
                            <!-- /.home-head -->
                            <div class="home-content pass-content col-xs-12">
                                <div class="home_data col-xs-12 pull-right text-right">
                                    <div class="interest-show">
                                        <ul>
                                            @if(! $user_interests == null)

                                                @foreach($user_interests as $user_interest)

                                                    <li>
                                                    <span class="inter-item">{{App\CourseCategory::where('id',$user_interest)->first()->title_ar}}
                                                        <a href="{{route('DELETE_INTEREST',$user_interest)}}"><i
                                                                    class="fa fa-times" id="del-item"></i></a>
                                                    </span>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                    <!-- /.interest-show -->
                                    <div class="add-interest">
                                        <a>
                                            <i class="fa fa-plus"></i> أضف اهتمامات أخري
                                        </a>
                                    </div>
                                    <!-- /.add-interest -->
                                    <form action="{{route('POST_ADD_INTEREST')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="home_data-item col-md-12  col-xs-12 pull-right">

                                            <div class="interest-cont col-xs-12">
                                                <!-- /.interest-item -->

                                                @foreach($interests as $interest)

                                                    <div class="interest-item col-md-4 col-sm-4 col-xs-6">

                                                        <label>
                                                            <input type="checkbox" name="interests[]"
                                                                   value="{{$interest->id}}">
                                                            <span>{{$interest->title_ar}}</span>

                                                        </label>

                                                    </div>
                                            @endforeach

                                            <!-- /.interest-item -->
                                            </div>
                                            <!-- /.interest-cont -->
                                            <div class="interst-gender col-xs-12">
                                                <h1>نوع الدورات التي تفضل متابعتها </h1>
                                                <div class="add_cont text-right">
                                                    <label class="text-right">
                                                        <input type="checkbox" name="interest_type" value="1">
                                                        <span>ذكور</span>
                                                    </label>
                                                    <label class="text-right">
                                                        <input type="checkbox" name="interest_type" value="2">
                                                        <span>إناث</span>
                                                    </label>
                                                </div>
                                                <div class="cv-file text-left">
                                                    <input type="submit" value="حفظ">
                                                </div>
                                            </div>
                                            <!-- /.interest-gender -->
                                        </div>
                                    </form>
                                    <!-- /.home_data-item -->
                                </div>
                                <!-- ./home_data -->
                            </div>
                            <!-- /.home-content -->
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="all-courses">
                            <div class="home-head">
                                <h1>
                                    <i class="fa fa-eye"></i>
                                    جميع الدورات
                                </h1>
                            </div>
                            <!-- /.home-head -->
                            <div class="home-content pass-content col-xs-12">
                                <div class="home_data col-xs-12 pull-right text-right">
                                    <div class="my_courses-container">
                                        <div>

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#current"
                                                                                          aria-controls="current"
                                                                                          role="tab" data-toggle="tab">الدورات
                                                        الحالية</a></li>
                                                {{--<li role="presentation"><a href="#comming" aria-controls="comming"--}}
                                                {{--role="tab" data-toggle="tab">الدورات--}}
                                                {{--القادمه</a></li>--}}

                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade in active" id="current">
                                                    @foreach($categories as $category)
                                                        <div class="type col-xs-12">
                                                            <div class="filtered-head text-right">
                                                                <h1>
                                                                    <i class="fa fa-tags"></i>
                                                                    {{$category->title_ar}}
                                                                </h1>
                                                            </div>
                                                            <!-- /.filtered-head -->
                                                            <?php
                                                            $courses = App\Course::where('cat_id', $category->id)->get();
                                                            $coursesCount = App\Course::where('cat_id', $category->id)->count();
                                                            ?>
                                                            @if(! $coursesCount == 0)

                                                                @foreach($courses as $course)
                                                                    <div class="card col-md-6 col-xs-12 pull-right">
                                                                        <div class="card-inner">
                                                                            <span class="corse-type">{{$course->pre_exp}}</span>
                                                                            <div class="card-img">

                                                                                <img src="{{asset('User/images/'.$course->course_image)}}"
                                                                                     alt=""
                                                                                     class="img-responsive">
                                                                                <div class="lessons-number text-center">
                                                                                    <h1>
                                                                                        <i class="fa fa-play-circle"></i>
                                                                                        <?php
                                                                                        $courseLectures = \Illuminate\Support\Facades\DB::table('lectures')->select('course_id')->where('course_id', $course->id)->count();
                                                                                        echo $courseLectures;
                                                                                        ?>
                                                                                    </h1>
                                                                                </div>
                                                                                <!-- /.lessons-number -->
                                                                            </div>
                                                                            <!-- /.card-img -->
                                                                            <div class="card-data">
                                                                                <div class="course_name text-right">
                                                                                    <h1>
                                                                                        <a href="{{route('GET_THIS_COURSE',$course->id)}}">                                                                                           {{$course->course_title}}
                                                                                        </a>
                                                                                    </h1>
                                                                                </div>
                                                                                <!-- /.course-name -->
                                                                                <div class="course_setting text-right">
                                                                            <span class="course_date">
                                                                                <i class="fa fa-calendar"></i>
                                                                                {{$course->course_date}}                                                                            </span>
                                                                                </div>
                                                                                <!-- /.course_setting -->
                                                                                <div class="course_instructor-data">
                                                                            <span>
                                                                                <?php
                                                                                $teacher = App\User::where('id', $course->teacher_id)->first();
                                                                                ?>
                                                                                <img src="{{asset('User/images/'.$teacher->Image)}}"
                                                                                     width="70" height="70"
                                                                                     class="img-responsive">
                                                                            </span>
                                                                                    <a href="#">
                                                                                        <i class="fa fa-user"></i>
                                                                                        {{$teacher->fullName}}
                                                                                    </a>
                                                                                </div>
                                                                                <!-- /.course_instructor-data -->
                                                                            </div>
                                                                            <!-- /.card-data -->

                                                                        </div>
                                                                        <!-- /.card-inner -->
                                                                    </div>
                                                                @endforeach
                                                            @else

                                                                <div role="tabpanel" class="tab-pane fade" id="comming">
                                                                    <div class="flash_empty text-center">
                                                                        <h1 class="animated shake">
                                                                            <i class="fa fa-frown-o"></i>
                                                                            عفواً لا يوجد لديك دورات في هذا القسم
                                                                        </h1>
                                                                    </div>
                                                                    <!-- /.flash_empty -->
                                                                </div>
                                                        @endif
                                                        <!-- /.card -->
                                                        </div>
                                                @endforeach
                                                <!-- /.type -->

                                                    <!-- /.type -->
                                                </div>
                                                <!-- /#current -->
                                                {{--<div role="tabpanel" class="tab-pane fade" id="comming">--}}
                                                {{--<div class="flash_empty text-center">--}}
                                                {{--<h1 class="animated shake">--}}
                                                {{--<i class="fa fa-frown-o"></i>--}}
                                                {{--عفواً لا يوجد لديك دورات في هذا القسم--}}
                                                {{--</h1>--}}
                                                {{--</div>--}}
                                                {{--<!-- /.flash_empty -->--}}
                                                {{--</div>--}}
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.my_courses-container -->
                                </div>
                                <!-- ./home_data -->
                            </div>
                            <!-- /.home-content -->
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="my_courses">
                            <div class="home-head">
                                <h1>
                                    <i class="fa fa-folder-open-o"></i>
                                    دوراتي
                                </h1>
                            </div>
                            <!-- /.home-head -->
                            <div class="home-content pass-content col-xs-12">
                                <div class="home_data col-xs-12 pull-right text-right">
                                    <div class="my_courses-container">
                                        <div>

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#currentmy"
                                                                                          aria-controls="current"
                                                                                          role="tab" data-toggle="tab">الدورات
                                                        الحالية</a></li>
                                                <li role="presentation"><a href="#commingmy" aria-controls="comming"
                                                                           role="tab" data-toggle="tab">الدورات
                                                        القادمة</a></li>
                                                <li role="presentation"><a href="#finishedmy" aria-controls="comming"
                                                                           role="tab" data-toggle="tab">الدورات
                                                        المنتهية</a></li>
                                            </ul>

                                            <!-- Tab panes -->


                                            <div class="tab-content">
                                                <!-- Current Courses -->


                                                <div role="tabpanel" class="tab-pane fade in active" id="currentmy">

                                                    @foreach($user->Courses as $course)
                                                        <div class="type col-xs-12">
                                                            <div class="filtered-head text-right">
                                                                <h1>
                                                                    <i class="fa fa-tags"></i>
                                                                    {{\App\CourseCategory::where('id',$course->cat_id)->first()->title_ar}}
                                                                </h1>
                                                            </div>
                                                            <!-- /.filtered-head -->
                                                            <div class="card col-md-6 col-xs-12 pull-right">
                                                                <div class="card-inner">
                                                                    <span class="corse-type">{{$course->pre_exp}}</span>
                                                                    <div class="card-img">

                                                                        <img src="{{asset('User/images/'.$course->course_image  )}}"
                                                                             alt=""
                                                                             class="img-responsive">
                                                                        <div class="lessons-number text-center">
                                                                            <h1>
                                                                                <i class="fa fa-play-circle"></i>
                                                                                <?php
                                                                                $courseLectures = \Illuminate\Support\Facades\DB::table('lectures')->select('course_id')->where('course_id', $course->id)->count();
                                                                                echo $courseLectures;
                                                                                ?>
                                                                            </h1>
                                                                        </div>
                                                                        <!-- /.lessons-number -->
                                                                    </div>
                                                                    <!-- /.card-img -->
                                                                    <div class="card-data">
                                                                        <div class="course_name text-right">
                                                                            <h1>
                                                                                <a href="{{route('GET_THIS_COURSE',$course->id)}}">{{$course->course_title}} </a>
                                                                            </h1>
                                                                        </div>
                                                                        <!-- /.course-name -->
                                                                        <div class="course_setting text-right">
                                                                            <span class="course_date">
                                                                                <i class="fa fa-calendar"></i>
                                                                                {{$course->course_date}}                                                                            </span>
                                                                        </div>
                                                                        <!-- /.course_setting -->
                                                                        <div class="course_instructor-data">
                                                                            <?php
                                                                            $teacher = App\User::where('id', $course->teacher_id)->first();
                                                                            ?>
                                                                            <span>
                                                                                <img src="{{asset('User/images/'.$teacher->Image)}}"
                                                                                     width="70" height="70"
                                                                                     class="img-responsive">
                                                                            </span>
                                                                            {{$teacher->fullName}}<i
                                                                                    class="fa fa-user"></i>
                                                                        </div>
                                                                        <!-- /.course_instructor-data -->
                                                                        <div class="corse-action">
                                                                            <a href="{{route('GET_THIS_COURSE',$course->id)}}"
                                                                               class="gonna-corse">
                                                                                <i class="fa fa-paper-plane"></i> إذهب
                                                                                الي
                                                                                الدورة
                                                                            </a>
                                                                            <a href="{{route('POST_REMOVE_COURSE_FROM_PIVOTE',$course->id)}}"
                                                                               class="out-corse">
                                                                                <i class="fa fa-sign-out"></i> إنسحاب من
                                                                                الدورة
                                                                            </a>
                                                                        </div>
                                                                        <!-- /.corse-action -->
                                                                    </div>
                                                                    <!-- /.card-data -->

                                                                </div>
                                                                <!-- /.card-inner -->
                                                            </div>
                                                            <!-- /.card -->
                                                        </div>
                                                        <!-- /.type -->
                                                    @endforeach
                                                </div>
                                                <!-- /#currentmy -->
                                                <div role="tabpanel" class="tab-pane fade" id="commingmy">
                                                    <div class="flash_empty text-center">
                                                        <h1 class="animated shake">
                                                            <i class="fa fa-frown-o"></i>
                                                            عفواً لا يوجد لديك دورات في هذا القسم
                                                        </h1>
                                                    </div>
                                                    <!-- /.flash_empty -->
                                                </div>
                                                <!-- /#commingmy -->
                                                <div role="tabpanel" class="tab-pane fade" id="finishedmy">
                                                    <div class="flash_empty text-center">
                                                        <h1 class="animated shake">
                                                            <i class="fa fa-frown-o"></i>
                                                            عفواً لا يوجد لديك دورات في هذا القسم
                                                        </h1>
                                                    </div>
                                                    <!-- /.flash_empty -->
                                                </div>
                                                <!-- /#finishedmy -->
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.my_courses-container -->
                                </div>
                                <!-- ./home_data -->
                            </div>
                            <!-- /.home-content -->
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="my_certf">
                            <div class="home-head">
                                <h1>
                                    <i class="fa fa-file"></i>
                                    الشهادات التي حصلت عليها من انهاء الدورات
                                </h1>
                            </div>
                            <!-- /.home-head -->
                            <div class="home-content pass-content col-xs-12">
                                <div class="home_data col-xs-12 pull-right text-right">
                                    <div class="home_data-item col-md-12  col-xs-12 pull-right">
                                        <div class="my-sertf">
                                            <ul>
                                                @if(! is_null(Auth::user()->Certificates))
                                                    @foreach(Auth::user()->Certificates as $certificate)
                                                        <li>
                                                            <h1>
                                                                <i class="fa fa-file"></i>
                                                                {{$certificate->certificate_name}}
                                                            </h1>
                                                            <a href="#">
                                                                <i class="fa fa-cloud-download"></i> تحميل الشهادة
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <h1>
                                                                <i class="fa fa-file"></i>
                                                                {{$certificate->certificate_name}}
                                                            </h1>
                                                            <a href="#">
                                                                <i class="fa fa-cloud-download"></i> تحميل الشهادة
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                        <!-- end my-certf -->
                                    </div>
                                    <!-- /.home_data-item -->
                                </div>
                                <!-- ./home_data -->
                            </div>
                            <!-- /.home-content -->
                        </div>
                    </div>
                    <!-- /.tap-content -->
                </div>
                <!-- /.left_tap-box -->
            </div>
            <!-- /.left_tap-box -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.profile-content -->

    <div class="courses">
        <div class="container">
            <div class="courses-head text-center">
                <h1>دوراتي </h1>
            </div>
            <!-- /.testominal-head -->
            <div class="row block-container">
                <?php
                $myCourses = DB::table('courses')->join('courses_users', function ($join) {
                    $join->on('courses.id', '=', 'courses_users.course_id')->where('courses_users.user_id', '=', Auth::id());
                })->get();
                //dd($myCourses);
                ?>
                {{--@foreach($user->Courses as $course)--}}


                @foreach($user->Courses as $course)
                    <div class="block col-md-4">
                        <figure>
                            <div><img src="{{asset('User/images/'.$course->course_image)}}" alt="img05"
                                      class="img-responsive"></div>
                            <figcaption class="text-right">
                                <h1>اسم الدوره : {{$course->course_title}}</h1>
                                <h1>اسم المدرس : {{App\User::where('id',$course->teacher_id)->first()->fullName}}</h1>
                                <h1>عدد الطلاب المشاركين : <?php
                                    $CourseStudents = \Illuminate\Support\Facades\DB::table('courses_users')->select('user_id')->where('course_id', $course->id)->count();
                                    echo $CourseStudents;
                                    ?>
                                </h1>
                                <h1>تاريخ بدايه الدوره : {{$course->course_date}}</h1>
                                <h1>تقييم الكورس</h1>
                                <a href="{{route('GET_THIS_COURSE',$course->id)}}">
                                    <i class="fa fa-eye"></i> مشاهدة الكورس
                                </a>
                            </figcaption>
                        </figure>
                    </div>
                @endforeach


            </div>
            <!-- /.row -->

            <div class="all-courses text-center">
                <a href="{{route('GET_ALL_COURSES')}}">عرض جميع الكورسات</a>

            </div>
            <!-- /.all-courses -->

        </div>
        <!-- /.conainer -->
    </div>
    <!-- /.courses -->
</div>

<footer>
    <div class="container">
        <div class="copyrights col-md-10 col-xs-12 text-center pull-right">
            <p>حميع الحقوق محفوظة لدي العلوم العصرية للتدريب</p>
        </div>
        <!-- /.copyrights -->
        <div class="footer-links col-md-2 col-xs-12 pull-left">
            <ul>
                <li>
                    <a href="#" data-toggle="tooltip" data-placement="top" title="facebook">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                </li>

                <li>
                    <a href="#" data-toggle="tooltip" data-placement="top" title="twitter">
                        <i class="fa fa-twitter-square"></i>
                    </a>
                </li>

                <li>
                    <a href="#" data-toggle="tooltip" data-placement="top" title="linkedin">
                        <i class="fa fa-linkedin-square"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.footer-links -->
    </div>
    <!-- /.container -->

</footer>
<!-- /.wrapper -->

<div class="toTop col-xs-12 text-center">
    <i class="fa fa-angle-up"></i>
</div>
<!-- /.toTop -->


<!-- Javascript Files -->
<script src="{{asset('User/js/jquery-2.2.2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/html5shiv.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery-smoothscroll.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/modernizr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/placeholdem.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/toucheffects.js')}}"></script>
<script src="{{asset('User/js/jquery.selectric.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/classie.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery.nicescroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/script.js')}}" type="text/javascript"></script>
</body>

</html>