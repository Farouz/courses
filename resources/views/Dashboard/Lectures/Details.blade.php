@extends('Dashboard.layout.master')
@section('content')
    <strong>
        <center>الملف الشخصي الخاص ب {{$lecture->lecture_title}}</center>
    </strong>
    <br>
    <br>
    <br>
    <strong>
        <center><p style="font-size: large">الفيديو الخاص بالدوره </p></center>
    </strong>
    <div class="home_img  text-center">
        <div class="home_img-inner">
            <div class="left-caption col-xs-12">
                <div class="imgcontent col-xs-12">
                    <div class="bstext">
                    </div>
                </div>
                <!-- /.imgcontent -->
            </div>
            <!-- /.left-caption -->
            <div>
                <div class="media">
                    <div class="media-body">
                        <?php
                        if (!$lecture->lecture_url == null) {
                            $video = Embed::make($lecture->lecture_url)->parseUrl();
                            if ($video) {
                                $video->setAttribute(['width' => 300]);
                                echo $video->getHtml();
                            }
                        } else {
                            $video = Embed::make($lecture->lecture_video)->parseUrl();
                            if ($video) {
                                $video->setAttribute(['width' => 300]);
                                echo $video->getHtml();
                            }
                        }

                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>



    <!-- /.home_img -->
    <div class="home-content">

        <div class="home_data col-md-10 col-sm-10 col-xs-12 text-right">
            <form action="#" method="get">
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-comment"></i>
                        <h1>اسم المحاضره</h1>

                        <span>{{$lecture->lecture_title}}</span>
                    </div>
                </div>
                <!-- /.home_data-item -->

                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-youtube-play"></i>
                        <h1>اسم الدوره</h1>
                        <span>{{App\Course::where('id',$lecture->course_id)->first()->course_title}}</span>
                    </div>
                </div>
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-user"></i>
                        <h1>اسم المدرس</h1>
                        <span>{{App\User::where('id',$lecture->teacher_id)->first()->fullName}}
                            &nbsp as &nbsp &nbsp {{App\User::where('id',$lecture->teacher_id)->first()->userName}} </span>
                    </div>
                </div>
                <!-- /.home_data-item -->
                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-hourglass"></i>
                        <h1>مده المحاضره</h1>

                        <span>{{$lecture->lecture_time}} Mins</span>
                    </div>
                </div>
                <!-- /.home_data-item -->




                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-calendar-check-o"></i>
                        <h1>وقت انشاء الدوره</h1>
                        <span>{{$lecture->created_at->toDateString()}}</span>
                    </div>
                </div>


                <div class="home_data-item all-set col-md-6 col-sm-6  col-xs-12 pull-right">
                    <div>
                        <i class="fa fa-commenting-o"></i>
                        <h1>عن الدوره</h1>
                        <div style="font-size: large;">{{$lecture->lecture_description}}</div>
                    </div>
                </div>

                <!-- /.home_data-item -->
            </form>
            <!-- /.home_data-item -->
        </div>
        <!-- ./home_data -->
        <!-- /.home-content -->
        <a href="{{route('GET_ALL_LECTURES_DASH')}}" style="float: left;"><button class="btn btn-success">Back To ALL lectures</button></a>
        <a href="{{route('GET_EDIT_LECTURE_DASH',$lecture->id)}}" style="float: right;"><button class="btn btn-success">Edit This lecture</button></a>
    </div>

@stop
