<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['Auth_Name', 'course_title', 'pre_exp', 'course_category', 'course_video', 'course_video_url', 'course_description', 'gender', 'course_payment_method', 'course_salary', 'course_image', 'teacher_id', 'course_date', 'course_end', 'certificate_name', 'certificate_branch', 'certificate_payment', 'certificate_salary'];

    public function Users()
    {
        return $this->belongsToMany(User::class, 'courses_users', 'course_id', 'user_id');
    }

    public function CourseCategory()
    {
        return $this->belongsTo(CourseCategory::class, 'cat_id');
    }

    public function Lectures()
    {
        return $this->hasMany(Lecture::class, 'course_id');
    }

    public function Notes()
    {
        return $this->hasMany(Note::class, 'course_id');
    }

    public function Discusses()
    {
        return $this->hasMany(Discuss::class, 'course_id');
    }

    public function Exams()
    {
        return $this->hasMany(Exam::class, 'course_id');
    }
}
