@extends('Dashboard.layout.master')
@section('content')
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">SETTINGS</h3>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Organization Name</th>
                        <th>Organization Image</th>
                        <th>FaceBook Account</th>
                        <th>Twitter Account</th>
                        <th>Linkden Account</th>
                        <td>Edit Setting</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($settings as $setting)
                        <tr>
                            <td>{{$setting->site_name}}</td>
                            <td>
                                <img src="{{asset('Public/User/images/'.$setting->site_image)}}" class="img-responsive"
                                     style="width: 400px;height: 150px">
                            </td>
                            <td>{{$setting->facebook}}</td>
                            <td>{{$setting->twitter}}</td>
                            <td>{{$setting->linkden}}</td>
                            <td>
                                <a href="{{route('EDIT_SETTING',$setting->id)}}"><i class="fa fa-edit"
                                                                                       data-toggle="tooltip"
                                                                                       style="color: darkblue"
                                                                                       title="EDIT SETTINGS"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            <br>
            <hr style="color: black;">
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>About Organization</th>
                        <th>About Image</th>
                        <th>Edit Settings</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($settings as $setting)
                        <tr>
                            <td>{{$setting->description}}</td>
                            <td style="width: 40%">
                                <img src="{{asset('Public/User/images/'.$setting->about_image)}}" class="img-responsive"
                                     style="width: 400px;height: 150px">
                            </td>

                            <td>
                                <a href="{{route('EDIT_SETTING',$setting->id)}}"><i class="fa fa-edit"
                                                                                    data-toggle="tooltip"
                                                                                    style="color: darkblue"
                                                                                    title="EDIT SETTINGS"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            <br>
            <hr style="color: black;">
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th> Organization Policy</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($settings as $setting)
                        <tr>
                            <td>{{$setting->policity}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div>
    </section>
@stop
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>