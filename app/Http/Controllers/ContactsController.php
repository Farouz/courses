<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Setting;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function getcontact()
    {
        $settings=Setting::all();
        return view('User.Contacts.contact',compact('settings'));
    }

    public function postcontact(Request $request)
    {
        $this->validate($request, ['sender_name' => 'required|regex:/^([^0-9]*)$/', 'sender_email' => 'required|email', 'message' => 'required']);
        $message = new Contact();
        $message->sender_name = $request->sender_name;
        $message->sender_email = $request->sender_email;
        $message->message = $request->message;
        $message->save();
        return back()->with('success', 'Message Sent');
    }
}
