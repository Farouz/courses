@extends('User.layout.master')
@section('Title')
    المناقشات
@stop
@section('content')
    <div class="intro-container col-xs-12">
        <div class="intro-head text-center">
            <div class="container">
                <h1>{{$course->course_title}}</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.intro-head -->

        <div class="corse-box col-xs-12">
            <div class="corse-nav text-center">
                <div class="container">
                    <ul>
                        <li>
                            <a href="{{route('GET_THIS_COURSE',$course->id)}}">
                                <i class="fa fa-tasks"></i> الدروس
                            </a>
                        </li>

                        <li>
                            <a href="{{route('GET_COURSE_DISCUSS',$course->id)}}" class="active">
                                <span class="padge">{{\App\Discuss::count()}}</span>
                                <i class="fa fa-commenting-o"></i> النقاشات
                            </a>
                        </li>

                        <li>
                            <a href="{{route('GET_COURSE_NOTES',$course->id)}}">
                                <span class="padge">{{\App\Note::count()}}</span>
                                <i class="fa fa-bell"></i> التنويهات
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- end container -->
            </div>
            <form action="{{route('POST_ADD_DISCUSS')}}" method="post" class="add-form" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <input type="hidden" name="course_id" value="{{$course->id}}">
                <div class="up_form-item">
                    <h1>اضافه مناقشه</h1>
                    <textarea name="discuss_body" placeholder="اضافة مناقشه جديده"></textarea>
                </div>
                <div class="up_form-item up-confirm">
                    <input class="btn btn-primary btn-lg btn-block" type="submit" value="اضافة مناقشه جديده">
                </div>
                <!-- /.up_form-item -->
            </form>
            <!-- end corse-nav -->
            <br>
            <button class="btn btn-secondary btn-lg btn-block ">جميع المناقشات</button>
            <div class="contact-box">
                <div class="alert-box">
                    <div class="all-alerts col-xs-12 text-right">
                        <ul>
                            @foreach($discusses as $discuss)
                                <li>
                                    <h1>{{\App\User::where('id',$discuss->user_id)->first()->fullName}}</h1>
                                    <span>{{$discuss->created_at->toDateString()}}</span>
                                    <p>
                                        {{$discuss->discuss_body}}
                                    </p>
                                </li>

                                <hr>
                            @endforeach
                        </ul>
                    </div>

                </div>
                <!-- end all-alerts -->
                <!--
                <div class="empty-msg text-center animated shake">
<h1>
                        <i class="fa fa-bell-slash"></i>
                        لا توجد تعليقات حتي الان
                    </h1>
</div>
-->
                <!-- end empty-msg -->
            </div>
        </div>


        <div class="lesson-box text-right">
            <div class="container">

                <div class="alert-box">
                    <!-- end empty-msg -->
                </div>
                @if(\App\Discuss::count()==0)
                    <div class="comments-disqus">
                        <div class="empty-msg text-center animated shake">
                            <h1>
                                <i class="fa fa-frown-o"></i>
                                لا يوجد دروس الان ولكن يمكنك الاشتراك في الدورة لحين بدأها
                            </h1>
                        </div>
                        <!-- end empty-msg -->
                    </div>
            @endif
            <!-- end comments-disqus -->
            </div>
            <!-- end container -->
        </div>
        <!-- end lesson-box -->
    </div>
    <!-- end corse-box -->

    </div>

@stop